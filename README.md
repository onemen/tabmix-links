# Tab Mix - Links

## About this extension
Tab Mix Plus WebExtensions is in its early development phase.  
**Tab Mix - Links** version 0.1.0 contain only "Links" features form legacy Tab mix plus

**<u>Important!</u>**  
  Read this: [The future of Tab Mix Plus](https://outgoing.prod.mozaws.net/v1/c140f1083582a56de50d89bb2d0f4fcb78a79a43646a489debf0e3117e700ea1/http%3A//tabmixplus.org/support/troubleshooting/data/legacy-tabmix.html)

**<u>Vote on Mozilla</u>**  
Vote on Mozilla website for APIs/bug fixes needed by Tab Mix Plus WebExtension.  
Read [here](https://outgoing.prod.mozaws.net/v1/141c48cd1e8766d3ffc4fa3a099230ed6fa20c8525c7ccba56b73fddf4613c88/http%3A//tabmixplus.org/forum/viewtopic.php%3Fp=73159%23p73159), about the bug list and how to vote.
  

----
## How to build

### Dependencies
The extension use [tldjs](https://www.npmjs.com/package/tldjs) as a replacement for Services.eTLD.getPublicSuffixFromHost

### Build process
Our build process uses webpack version 3.10.0 to import tldjs into a background script - index.js,  
and [web-ext](https://www.npmjs.com/package/web-ext) to create the extension package.  
It contains 2 scripts:  
run ```npm run build``` to create the bundled background script.  
run ```npm run xpi``` to create the extension package.  
