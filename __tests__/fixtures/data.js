export const data = {
  wrappedNode: {
    __tabmix: true,
    baseURI: 'https://www.test_base.com/',
    host: 'tabmixplus.org',
    pathname: '/',
    className: 'link',
    target: '_blank',
    ownerDocument: {
      URL: 'http://localhost/test_page.html',
      documentURI: 'http://localhost/test_page.html',
      defaultView: {frameElement: false},
      location: {href: 'http://localhost/test_page.html'},
    },
    parentNode: {
      baseURI: 'https://www.test_base.com/',
      _attributes: {onclick: 'parent_onclick_function();'},
    },
    _focusedWindowHref: 'http://localhost/test_page.html',
    _attributes: {
      href: 'http://tabmixplus.org',
      onclick: 'onclick_function();',
      onmousedown: 'onmousedown_function();',
      rel: 'sidebar',
      role: 'button',
    },
    targetIsFrame: false,
  },

  message: {
    event: {
      altKey: false,
      bookmark: false,
      button: 0,
      ctrlKey: false,
      isContentWindowPrivate: false,
      metaKey: false,
      originAttributes: {},
      shiftKey: false,
      title: null,
    },
    href: 'https://wiki.a_tag.com/',
    node: {
      __tabmix: true,
      _attributes: {href: 'https://wiki.a_tag.com'},
      _focusedWindowHref: 'http://localhost/',
      baseURI: 'http://localhost/',
      className: '',
      host: 'wiki.a_tag.com',
      ownerDocument: {
        URL: 'http://localhost/',
        defaultView: {frameElement: false},
        documentURI: 'http://localhost/',
        location: {href: 'http://localhost/'},
      },
      parentNode: {
        _attributes: { },
        baseURI: 'http://localhost/',
      },
      pathname: '/',
      target: '',
      targetIsFrame: false,
    },
  },
};

Object.defineProperty(data, 'messageJson', {
  get() {
    return JSON.parse(JSON.stringify(this.message));
  },
  enumerable: false,
  configurable: false,
});
