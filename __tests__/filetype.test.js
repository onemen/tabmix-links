/**
* @jest-environment jsdom
*/
/* globals global */
import {mockMethods} from './_helpers.js';

let filetypeModal, mockedMethods, filetypePref;
const $ = id => document.getElementById(id);

// read preferences.html
const fs = require('fs');
const html = fs.readFileSync('./addon/preferences/preferences.html', 'utf8');
const bodyHtml = /<body.*?>([\s\S]*)<\/body>/.exec(html)[1];
document.body.innerHTML = bodyHtml.split('<!-- Modal -->')[1];

beforeAll(() => {
  global.$ = $;
  global.prefWindow = {setDisabled: jest.fn()};
  global.openHelp = jest.fn();

  const storageRewireAPI = require('../addon/common/storage.js');
  global.defaultValues = storageRewireAPI.__GetDependency__('defaultValues');
  filetypePref = global.defaultValues.filetype;

  browser.storage.local.get = jest.fn(name => {
    if (name == 'filetype') {
      return Promise.resolve({filetype: filetypePref});
    }
    return Promise.resolve({});
  });

  filetypeModal = require('../addon/preferences/filetype.js');
  mockedMethods = mockMethods.bind(null, filetypeModal);
});

afterEach(() => {
  if (filetypeModal.modal == $('filetype-modal')) {
    filetypeModal.close();
  }
  jest.clearAllMocks();
});

function setNewEntryValue(val) {
  filetypeModal.newEntry = $('filetype-entry');
  filetypeModal.newEntry.value = val;
}

function createList() {
  const list = $('filetype-list');
  list.innerHTML =
    '<option value="aaa">aaa</option>' +
    '<option value="bbb">bbb</option>' +
    '<option value="/download\\.asp/">/download\\.asp/</option>';
  return list;
}

function clearList() {
  Array.from(filetypeModal.list.childNodes).forEach(item => item.remove());
  filetypeModal.newEntry = null;
  filetypeModal.empty = null;
  filetypeModal.list = null;
}

function runTestsOnMethods(list, expected, functionToTest, moreTests) {
  return mockedMethods(list, mocked => {
    function runTests() {
      list.forEach(name => {
        expect(mocked[name]).toBeCalledTimes(expected[name] || 0);
      });
      if (moreTests) {
        moreTests(mocked);
      }
    }

    let result;
    try {
      result = functionToTest();
      if (Boolean(result) && typeof result.then == 'function') {
        return result
            .then(runTests)
            .catch(runTests);
      }
      runTests();
    } catch (ex) {
      runTests();
      throw ex;
    }
    return result;
  });
}

describe('filetypeModal', () => {
  it('open should call other methods', () => {
    return mockedMethods(['populateFiletypeList', 'selectItemAt'], async mocked => {
      const initSpy = jest.spyOn(filetypeModal, 'init');
      // before modal open
      expect(filetypeModal.modal).toBeUndefined();
      expect(filetypeModal.empty).toBeNull();

      await filetypeModal.open();

      // after modal open
      const {populateFiletypeList, selectItemAt} = mocked;
      expect(initSpy).toBeCalledTimes(1);
      expect(populateFiletypeList).toBeCalledTimes(1);
      expect(selectItemAt).toBeCalledTimes(1);
      expect(selectItemAt).toBeCalledWith(0, true);
      expect(filetypeModal.empty).toBe(false);
      expect(filetypeModal.modal.style.display).toBe('block');
    });
  });

  it('init should initialize event listeners and get dom elements', () => {
    const spies = ['modal', 'list', 'entry'].map(item => {
      return jest.spyOn($(`filetype-${item}`), 'addEventListener');
    });
    filetypeModal.init();

    expect(filetypeModal.modal).toEqual($('filetype-modal'));
    expect(filetypeModal.closeBtn).toEqual($('filetype-modal').getElementsByClassName('close')[0]);
    expect(filetypeModal.list).toEqual($('filetype-list'));
    expect(filetypeModal.newEntry).toEqual($('filetype-entry'));

    expect(spies[0]).toBeCalledTimes(2);
    expect(spies[0]).toBeCalledWith('click', filetypeModal);
    expect(spies[0]).toBeCalledWith('keypress', filetypeModal);
    expect(spies[1]).toBeCalledTimes(1);
    expect(spies[1]).toBeCalledWith('change', filetypeModal);
    expect(spies[2]).toBeCalledTimes(1);
    expect(spies[2]).toBeCalledWith('input', filetypeModal);
    spies.forEach(spy => spy.mockRestore());
  });

  describe('handleEvent', () => {
    const methodList = [
      'updateSelectedItem',
      'handleClick',
      'input',
      'save',
      'close',
    ];

    function runHandleEventTest(event, expected = {}, moreTests) {
      return runTestsOnMethods(methodList, expected,
          () => filetypeModal.handleEvent(event),
          moreTests
      );
    }

    it('should call updateSelectedItem', () => {
      runHandleEventTest({type: 'change'}, {updateSelectedItem: 1});
    });

    it('should call handleClick', () => {
      const target = document.createElement('div');
      runHandleEventTest({target, type: 'click'}, {handleClick: 1}, () => {
        expect(filetypeModal.handleClick).toBeCalledWith({target, type: 'click'});
      });
    });

    it('should call input', () => {
      runHandleEventTest({type: 'input'}, {input: 1});
    });

    it('should call save and close', () => {
      runHandleEventTest({type: 'keypress', keyCode: 13}, {save: 1, close: 1});
    });

    it('should NOT call save and close', () => {
      runHandleEventTest({type: 'keypress', keyCode: 11}, {save: 0, close: 0});
    });

    it('should call close', () => {
      runHandleEventTest({type: 'keypress', keyCode: 27}, {close: 1});
    });

    it('should throws on unknown event', () => {
      expect(() => {
        runHandleEventTest({type: 'xxxxx'});
      }).toThrow('Tabmix Error: unknown event xxxxx');
    });
  });

  describe('handleClick', () => {
    const target = document.createElement('button');
    const methodList = [
      'close',
      'add',
      'mod',
      'del',
      'restore',
      'save',
    ];

    function runHandleClickTest(event, expected = {}) {
      return runTestsOnMethods(methodList, expected,
          () => filetypeModal.handleClick(event),
          () => expect(global.openHelp).toBeCalledTimes(expected.openHelp || 0)
      );
    }

    it('should NOT call any function when event.target is not a button', () => {
      runHandleClickTest({target: document.createElement('input')});
    });

    it('should NOT call any function when event.target.id is not in the switch cases', () => {
      target.id = 'xxxxx';
      runHandleClickTest({target});
    });

    it('should close the modal', () => {
      filetypeModal.modal = target;
      runHandleClickTest({target}, {close: 1});

      filetypeModal.modal = null;
      filetypeModal.closeBtn = target;
      runHandleClickTest({target}, {close: 1});

      filetypeModal.closeBtn = null;
    });

    it('should call add', () => {
      target.id = 'filetype-add';
      runHandleClickTest({target}, {add: 1});
    });

    it('should call mod', () => {
      target.id = 'filetype-edit';
      runHandleClickTest({target}, {mod: 1});
    });

    it('should call del', () => {
      target.id = 'filetype-delete';
      runHandleClickTest({target}, {del: 1});
    });

    it('should call restore', () => {
      target.id = 'filetype-restore';
      runHandleClickTest({target}, {restore: 1});
    });

    it('should call save and close', () => {
      target.id = 'filetype-save';
      runHandleClickTest({target}, {save: 1, close: 1});
    });

    it('should call close', () => {
      target.id = 'filetype-close';
      runHandleClickTest({target}, {close: 1});
    });

    it('should call openHelp', () => {
      target.id = 'filetype-help';
      target.setAttribute('helpTopic', 'test_help_topic');
      runHandleClickTest({target}, {openHelp: 1});
      expect(global.openHelp).toBeCalledWith('test_help_topic');
    });
  });

  it('close should remove event listeners and reset internals to null', async() => {
    await filetypeModal.init();

    expect(filetypeModal.modal).toEqual($('filetype-modal'));
    expect(filetypeModal.closeBtn).toEqual($('filetype-modal').getElementsByClassName('close')[0]);
    expect(filetypeModal.list).toEqual($('filetype-list'));
    expect(filetypeModal.newEntry).toEqual($('filetype-entry'));

    const spies = ['modal', 'list', 'entry'].map(item => {
      return jest.spyOn($(`filetype-${item}`), 'removeEventListener');
    });

    filetypeModal.close();

    expect(filetypeModal.modal).toBeNull();
    expect(filetypeModal.closeBtn).toBeNull();
    expect(filetypeModal.list).toBeNull();
    expect(filetypeModal.newEntry).toBeNull();
    expect(spies[0]).toBeCalledTimes(2);
    expect(spies[0]).toBeCalledWith('click', filetypeModal);
    expect(spies[0]).toBeCalledWith('keypress', filetypeModal);
    expect(spies[1]).toBeCalledTimes(1);
    expect(spies[1]).toBeCalledWith('change', filetypeModal);
    expect(spies[2]).toBeCalledTimes(1);
    expect(spies[2]).toBeCalledWith('input', filetypeModal);
    spies.forEach(spy => spy.mockRestore());
  });

  it('defaultValue should be preferences.filetype', () => {
    expect(filetypeModal.defaultValue).toEqual(global.defaultValues.filetype);
  });

  describe('populateFiletypeList', () => {
    let originalFiletypePref;
    beforeAll(() => {
      originalFiletypePref = global.defaultValues.filetype;
    });

    afterAll(() => {
      global.defaultValues.filetype = originalFiletypePref;
      filetypePref = originalFiletypePref;
      filetypeModal.defaultValue = originalFiletypePref;
    });

    const methodList = ['setButtonDisable', 'insertPlaceholder'];

    function runPopulateFiletypeListTest(restore, expected = {}, moreTests) {
      return runTestsOnMethods(methodList, expected,
          () => filetypeModal.populateFiletypeList(restore),
          moreTests
      );
    }

    it('should disable delete button when list is empty', async() => {
      filetypePref = '';
      await runPopulateFiletypeListTest(null, {setButtonDisable: 1, insertPlaceholder: 1}, () => {
        expect(filetypeModal.setButtonDisable).toBeCalledWith('delete', true);
      });
    });

    it('should disable delete button when default list is empty', async() => {
      filetypeModal.defaultValue = '';
      await runPopulateFiletypeListTest(true, {setButtonDisable: 1, insertPlaceholder: 1}, () => {
        expect(filetypeModal.setButtonDisable).toBeCalledWith('delete', true);
      });
    });

    it('should populate list from preferences.filetype', async() => {
      filetypePref = 'aaa bbb /download\\.php/';
      filetypeModal.defaultValue = 'ccc ddd /download\\.asp/';

      await filetypeModal.open();
      await runPopulateFiletypeListTest();
      const list = filetypeModal.list.childNodes;
      expect(list).toHaveLength(3);
      expect(list[0].outerHTML).toBe('<option value="aaa">aaa</option>');
      expect(list[1].outerHTML).toBe('<option value="bbb">bbb</option>');
      expect(list[2].outerHTML).toBe('<option value="/download\\.php/">/download\\.php/</option>');
    });

    it('should populate list from defaultValues.filetype', async() => {
      filetypePref = 'aaa bbb /download\\.php/';
      filetypeModal.defaultValue = 'ccc ddd /download\\.asp/';

      await filetypeModal.open();
      await runPopulateFiletypeListTest(true);
      const list = filetypeModal.list.childNodes;
      expect(list).toHaveLength(3);
      expect(list[0].outerHTML).toBe('<option value="ccc">ccc</option>');
      expect(list[1].outerHTML).toBe('<option value="ddd">ddd</option>');
      expect(list[2].outerHTML).toBe('<option value="/download\\.asp/">/download\\.asp/</option>');
    });
  });

  describe('save', () => {
    it('should update our storage', () => {
      filetypeModal.empty = false;
      filetypeModal.list = createList();

      filetypeModal.save();
      expect(browser.storage.local.set).toBeCalledTimes(1);
      expect(browser.storage.local.set).toBeCalledWith({filetype: 'aaa bbb /download\\.asp/'});
      clearList();
    });

    it('should update our storage with empty string', () => {
      // browser.storage.local.set.mockClear();
      filetypeModal.empty = true;
      filetypeModal.save();
      expect(browser.storage.local.set).toBeCalledTimes(1);
      expect(browser.storage.local.set).toBeCalledWith({filetype: ''});
    });

    // it('should log an error with console.error', async() => {
    //   const expectedErrorMsg = 'Tabmix: Error occurred when trying to save filetype preference\n';
    //   const expectedError = new Error('test error');
    //   browser.storage.local.set = jest.fn(() => {
    //     return Promise.reject(new Error('test error'));
    //   });

    //   const originalConsoleError = global.console.error;
    //   global.console.error = jest.fn();

    //   await filetypeModal.save();
    //   expect(global.console.error).toBeCalledTimes(1);
    //   expect(global.console.error).toBeCalledWith(expectedErrorMsg, expectedError);
    //   global.console.error = originalConsoleError;
    // });

    it('should log an error with console.error', async() => {
      const expectedErrorMsg = 'Tabmix: Error occurred when trying to save filetype preference\n';
      const expectedError = new Error('test error');
      const localSet = browser.storage.local.set;
      browser.storage.local.set = jest.fn(() => {
        return Promise.reject(new Error('test error'));
      });
      const errorLogSpy = jest.spyOn(global.console, 'error').mockImplementation(jest.fn());

      await filetypeModal.save();
      expect(errorLogSpy).toBeCalledTimes(1);
      expect(errorLogSpy).toBeCalledWith(expectedErrorMsg, expectedError);

      browser.storage.local.set = localSet;
      errorLogSpy.mockRestore();
    });
  });

  describe('updateSelectedItem', () => {
    function runUpdateSelectedItemTest(select) {
      filetypeModal.list = createList();
      filetypeModal.newEntry = document.createElement('input');

      if (select) {
        filetypeModal.list.selectedIndex = 1;
      }

      mockedMethods('setButtonDisable', ({setButtonDisable}) => {
        filetypeModal.updateSelectedItem();
        expect(setButtonDisable).toBeCalledTimes(3);
        expect(setButtonDisable.mock.calls[0]).toEqual(['edit', true]);
        expect(setButtonDisable.mock.calls[1]).toEqual(['add', true]);
        expect(setButtonDisable.mock.calls[2]).toEqual(['delete', !select]);
        expect(filetypeModal.newEntry.value).toEqual(select ? 'bbb' : '');
        clearList();
      });
    }

    it('should NOT sets the textbox when no item selected in the list', () => {
      runUpdateSelectedItemTest();
    });

    it('should sets the textbox with the selected item value', () => {
      runUpdateSelectedItemTest(true);
    });
  });

  describe('add', () => {
    function runAddTest(newEntryValue, index) {
      setNewEntryValue(newEntryValue);
      mockedMethods(['selectItemAt', 'setButtonDisable'], ({selectItemAt, setButtonDisable}) => {
        filetypeModal.add();
        expect(selectItemAt).toBeCalledTimes(newEntryValue ? 1 : 0);
        expect(setButtonDisable).toBeCalledTimes(newEntryValue ? 1 : 0);
        if (newEntryValue) {
          expect(selectItemAt).toBeCalledWith(index, true);
          expect(setButtonDisable).toBeCalledWith('delete', false);
          const item = filetypeModal.list.childNodes[index];
          expect(item.value).toBe(newEntryValue);
          expect(item.text).toBe(newEntryValue);
        }
        clearList();
      });
    }

    it('should NOT add new option when the input filed in empty', () => {
      filetypeModal.list = $('filetype-list');
      runAddTest('');
    });

    it('should add first new option to an empty list', () => {
      filetypeModal.list = $('filetype-list');
      filetypeModal.empty = true;
      runAddTest('new_value', 0);
    });

    it('should append new value to the list', () => {
      filetypeModal.list = createList();
      filetypeModal.empty = false;
      runAddTest('new_value_with_index_3', 3);
    });

    it('should append new value with "\\" to the list', () => {
      filetypeModal.list = createList();
      filetypeModal.empty = false;
      runAddTest('/download\\/file\\.zip/', 3);
    });
  });

  describe('mod', () => {
    function runModTest(newEntryValue, index, selected) {
      setNewEntryValue(newEntryValue);
      mockedMethods(['add', 'selectItemAt', 'setButtonDisable'], ({add, selectItemAt, setButtonDisable}) => {
        filetypeModal.mod();
        expect(add).toBeCalledTimes(newEntryValue && !selected ? 1 : 0);
        expect(selectItemAt).toBeCalledTimes(newEntryValue && selected ? 1 : 0);
        expect(setButtonDisable).toBeCalledTimes(newEntryValue && selected ? 2 : 0);
        if (newEntryValue && selected) {
          expect(selectItemAt).toBeCalledWith(index, false);
          expect(setButtonDisable).toBeCalledWith('edit', true);
          expect(setButtonDisable).toBeCalledWith('add', true);
          const item = filetypeModal.list.childNodes[index];
          expect(item.value).toBe(newEntryValue);
          expect(item.text).toBe(newEntryValue);
        }
        clearList();
      });
    }

    it('should NOT add modify an option when the input filed in empty', () => {
      filetypeModal.list = $('filetype-list');
      runModTest('');
    });

    it('should add new item when no item selected', () => {
      filetypeModal.list = $('filetype-list');
      filetypeModal.empty = false;
      runModTest('added_item', 3);
    });

    it('should modified selected item', () => {
      filetypeModal.list = createList();
      filetypeModal.empty = false;
      filetypeModal.list.selectedIndex = 1;
      runModTest('modified_item', 1, true);
    });
  });

  describe('input', () => {
    function runInputTest(newEntryValue, buttonsState, item, select) {
      filetypeModal.list = createList();
      if (select) {
        filetypeModal.list.selectedIndex = 0;
      }
      setNewEntryValue(newEntryValue);
      mockedMethods(['selectItemAt', 'setButtonDisable'], ({selectItemAt, setButtonDisable}) => {
        filetypeModal.input();
        expect(selectItemAt).toBeCalledTimes(item ? 1 : 0);
        expect(setButtonDisable).toBeCalledTimes(buttonsState.length);
        if (item) {
          expect(selectItemAt).toBeCalledWith(item.index, false);
        }
        expect(setButtonDisable).toBeCalledWith('add', buttonsState[0]);
        if (buttonsState.length == 2) {
          expect(setButtonDisable).toBeCalledWith('edit', buttonsState[1]);
        }
        clearList();
      });
    }

    it('should disable edit and add buttons', () => {
      runInputTest('', [true, true]);
    });

    it('should disable edit and add buttons and select item when the value in the input exist in the list', () => {
      runInputTest('aaa', [true, true], {index: 0});
      runInputTest('bbb', [true, true], {index: 1});
      runInputTest(String('/download\\.asp/'), [true, true], {index: 2});
    });

    it('should enable only add button when no item selected and the input value does not exist', () => {
      runInputTest('new_value', [false]);
    });

    it('should enable add and edit buttons when an item was selected and the input value does not exist', () => {
      runInputTest('new_value', [false, false], null, true);
    });
  });

  describe('del', () => {
    function runDelTest(item, isLastItem) {
      if (item) {
        filetypeModal.list.selectedIndex = item.selected;
      }

      const insertPlaceholder = jest.spyOn(filetypeModal, 'insertPlaceholder');
      mockedMethods(['selectItemAt', 'setButtonDisable'], ({selectItemAt, setButtonDisable}) => {
        filetypeModal.del();
        expect(selectItemAt).toBeCalledTimes(!item || isLastItem ? 0 : 1);
        expect(setButtonDisable).toBeCalledTimes(item && isLastItem ? 1 : 0);
        expect(insertPlaceholder).toBeCalledTimes(item && isLastItem ? 1 : 0);
        expect(filetypeModal.empty).toBe(isLastItem);
        if (item) {
          if (isLastItem) {
            expect(setButtonDisable).toBeCalledWith('delete', true);
            expect(filetypeModal.newEntry.value).toBe('');
            expect(filetypeModal.list.innerHTML).toBe('<option style="visibility:hidden;" value="0">0</option>');
          } else {
            expect(selectItemAt).toBeCalledWith(item.next, true);
          }
        }
        const count = !item ? 3 : isLastItem ? 1 : 2;
        expect(filetypeModal.list.childNodes).toHaveLength(count);
        clearList();
        insertPlaceholder.mockRestore();
      });
    }

    it('should NOT delete and item when no item selected', () => {
      filetypeModal.empty = false;
      filetypeModal.list = createList();
      runDelTest(null, false);
    });

    it('should delete selected and select next item', () => {
      filetypeModal.empty = false;
      filetypeModal.list = createList();
      runDelTest({selected: 0, next: 1}, false);
    });

    it('should delete item in the last index and select item before it', () => {
      filetypeModal.empty = false;
      filetypeModal.list = createList();
      runDelTest({selected: 2, next: 1}, false);
    });

    it('should delete last item in the list and add placeholder', () => {
      filetypeModal.newEntry = $('filetype-entry');
      filetypeModal.list = createList();
      filetypeModal.empty = false;
      filetypeModal.list.firstChild.remove();
      filetypeModal.list.firstChild.remove();
      filetypeModal.newEntry.value = 'xxxxxx';
      runDelTest({selected: 0}, true);
    });
  });

  it('restore should reset filetype preferences to default', () => {
    // look at populateFiletypeList and save complete tests above
    const methodList = ['populateFiletypeList', 'selectItemAt', 'save'];
    const expected = {populateFiletypeList: 1, selectItemAt: 1, save: 1};
    runTestsOnMethods(methodList, expected,
        () => filetypeModal.restore(),
        mocked => {
          expect(mocked.populateFiletypeList).toBeCalledWith(true);
          expect(mocked.selectItemAt).toBeCalledWith(0, true);
        }
    );
  });
});
