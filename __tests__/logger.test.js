/* globals global */
// import {getPreferences, resetPreferences, rewireWith} from './_helpers.js';
import {__RewireAPI__ as loggerRewireAPI} from '../addon/common/logger';

// const testFunctions = rewireWith.bind(null, loggerRewireAPI);

// let preferences;

// beforeAll(() => {
//   preferences = getPreferences();
//   preferences.pref_number = 10;
//   preferences.pref_boolean = false;
// });

// afterAll(() => {
//   resetPreferences();
// });

afterEach(() => {
  jest.clearAllMocks();
  jest.clearAllMocks();
});

const console_log = global.console.log;

afterEach(() => {
  global.console.log = console_log;
  loggerRewireAPI.__ResetDependency__('logging');
  loggerRewireAPI.__ResetDependency__('debugging');
});

describe('logger', () => {
  const getName = loggerRewireAPI.__GetDependency__('getName');
  const log = loggerRewireAPI.__GetDependency__('log');
  const debug = loggerRewireAPI.__GetDependency__('debug');

  function testFunction(...msg) {
    return getName(...msg);
  }

  it('getName should return function name', () => {
    expect(testFunction()).toContain('testFunction');
  });

  it('log should not call console.log when logging is false', () => {
    global.console.log = jest.fn();
    loggerRewireAPI.__Rewire__('logging', false);
    log('test', 'log', 'function');
    expect(global.console.log).toBeCalledTimes(0);
  });

  it('debug should not call console.log when debugging is false', () => {
    global.console.log = jest.fn();
    loggerRewireAPI.__Rewire__('debugging', false);
    debug('test', 'debug', 'function');
    expect(global.console.log).toBeCalledTimes(0);
  });

  it('log should call console.log when logging is true', () => {
    global.console.log = jest.fn();
    loggerRewireAPI.__Rewire__('logging', true);
    log('test', 'log', 'function');
    expect(global.console.log).toBeCalledTimes(1);
    expect(global.console.log.mock.calls[0][0]).toContain('log');
    expect(global.console.log.mock.calls[0][1]).toEqual('test');
    expect(global.console.log.mock.calls[0][2]).toEqual('log');
    expect(global.console.log.mock.calls[0][3]).toEqual('function');
  });

  it('debug should call console.log when debugging is true', () => {
    global.console.log = jest.fn();
    loggerRewireAPI.__Rewire__('debugging', true);
    debug('test', 'debug', 'function');
    expect(global.console.log).toBeCalledTimes(1);
    expect(global.console.log.mock.calls[0][0]).toContain('debug');
    expect(global.console.log.mock.calls[0][1]).toEqual('test');
    expect(global.console.log.mock.calls[0][2]).toEqual('debug');
    expect(global.console.log.mock.calls[0][3]).toEqual('function');
  });
});
