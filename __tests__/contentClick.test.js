/**
* @jest-environment jsdom
*/
/* globals global */
import {delayed, rewireWith} from './_helpers.js';
import {data} from './fixtures/data.js';
import {__RewireAPI__ as contentRewireAPI} from '../addon/content_scripts/contentClick.js';

const testFunctions = rewireWith.bind(null, contentRewireAPI);
const nodes = contentRewireAPI.__GetDependency__('nodes');

beforeAll(() => {
  const loggerRewireAPI = require('../addon/common/logger.js');
  global.log = loggerRewireAPI.__GetDependency__('log');
  global.debug = loggerRewireAPI.__GetDependency__('debug');

  // read contentClick.html
  const fs = require('fs');
  const html = fs.readFileSync('./__tests__/fixtures/contentClick.html', 'utf8');
  const bodyHtml = /<body.*?>([\s\S]*)<\/body>/.exec(html)[1];
  const base = /(<base[\s\S]*\/>)/.exec(html)[1];

  document.head.innerHTML = base;
  document.body.innerHTML = bodyHtml;
});

beforeEach(() => {
  jest.clearAllMocks();
});

// navigation (except hash changes) does not implemented in jsdom
// for changes in current tab we change:
// from: http://localhost
// to:   http://localhost/#new_href
function saveNodeData(node, {notLink = false, where = 'tab'} = {}) {
  const savedData = {
    attributes: [
      ['onclick', 'onclick_function'],
      ['target', '_self'],
      ['href', 'https://wiki.mozilla.org'],
    ],
    href: 'http://localhost/#new_href',
    notLink,
    where,
  };
  nodes.set(document.documentElement, {node, data: savedData});
  return savedData;
}

describe('LinkNodeUtils', () => {
  const LinkNodeUtils = contentRewireAPI.__GetDependency__('LinkNodeUtils');
  // isFrameInContent(content, href, name)
  it('isFrameInContent should return false when content is missing', () => {
    let result = LinkNodeUtils.isFrameInContent(null, 'href', 'name');
    expect(result).toEqual(false);
    result = LinkNodeUtils.isFrameInContent();
    expect(result).toEqual(false);
  });

  it('isFrameInContent should return false when href or name are not matched', () => {
    const content1 = {
      name: 'frame-name1',
      location: {href: 'frame-href'},
      frames: [{
        name: 'frame-name2',
        location: {href: 'frame-href'},
      }],
    };
    const result1 = LinkNodeUtils.isFrameInContent(content1, 'frame-href', 'frame-name');
    expect(result1).toEqual(false);

    const content2 = {
      name: 'frame-name',
      location: {href: 'frame-href1'},
      frames: [{
        name: 'frame-name',
        location: {href: 'frame-href2'},
      }],
    };
    const result2 = LinkNodeUtils.isFrameInContent(content2, 'frame-href', 'frame-name');
    expect(result2).toEqual(false);
  });

  it('isFrameInContent should return true when href and name are matched', () => {
    const content1 = {
      name: 'frame-name',
      location: {href: 'frame-href'},
    };
    const result1 = LinkNodeUtils.isFrameInContent(content1, 'frame-href', 'frame-name');
    expect(result1).toEqual(true);

    const content2 = {
      name: 'frame-name',
      location: {href: 'frame-href1'},
      frames: [{
        name: 'frame-name',
        location: {href: 'frame-href'},
      }],
    };
    const result2 = LinkNodeUtils.isFrameInContent(content2, 'frame-href', 'frame-name');
    expect(result2).toEqual(true);
  });

  // wrap(node, focusedWindow, getTargetIsFrame)
  it('wrap should bail out when node is missing', () => {
    expect(LinkNodeUtils.wrap()).toBeUndefined();
    expect(LinkNodeUtils.wrap(null)).toEqual(null);
    expect(LinkNodeUtils.wrap(undefined)).toBeUndefined();
  });

  it('wrap should return same node when it is already wrapped', () => {
    const node = {
      __tabmix: true,
      prop1: 'prop1',
      prop2: 'prop2',
    };
    expect(LinkNodeUtils.wrap(node) === node).toBe(true);
  });

  it('wrap should return wrapped node', () => {
    window.history.pushState({}, 'Test Title', 'http://localhost/test_page.html');

    const node = document.getElementById('tabmixplus');
    expect(LinkNodeUtils.wrap(node, window, true)).toEqual(data.wrappedNode);
  });

  it('getNodeWithOnClick should return node with onclick', () => {
    const node1 = document.getElementById('node_without_href');
    const expectedResult = document.getElementById('node_with_onclick');
    expect(LinkNodeUtils.getNodeWithOnClick(node1)).toEqual(expectedResult);

    // getNodeWithOnClick look only 3 level up
    const node2 = document.getElementById('deeper_node_without_href');
    expect(LinkNodeUtils.getNodeWithOnClick(node2)).toEqual(null);
  });

  // isSpecialPage(href, linkNode, currentHref, window)
  it('isSpecialPage should return true', () => {
    const list = [
      'www.duckduckgo.com',
      'www.tvguide.com',
      'www.google.com',
      'www.yahoo.com',
      'www.duckduckgo.com',
      'www.jetbrains.com',
    ];
    list.forEach(href => {
      expect(LinkNodeUtils.isSpecialPage('', {}, href)).toBe(true);
    });
    // expect(LinkNodeUtils.isSpecialPage('', {}, 'www.address.com')).toBe(false);
    expect(LinkNodeUtils.isSpecialPage('', {}, window.location.href)).toBe(false);
  });

  it('isSpecialPage should return true for youtube.com only when link is not Greasemonkey script', () => {
    const currentHref = 'http://www.youtube.com';
    expect(LinkNodeUtils.isSpecialPage('', {}, currentHref, window)).toBe(true);

    LinkNodeUtils._GM_function.set(window, () => false);
    expect(LinkNodeUtils.isSpecialPage('', {}, currentHref, window)).toBe(true);

    // TODO - check this test again when we start using _GM_function
    // we don't don't block links on youtube.com when Greasemonkey installed and
    // href contains 'return false;'
    LinkNodeUtils._GM_function.set(window, () => true);
    expect(LinkNodeUtils.isSpecialPage('', {}, currentHref, window)).toBe(true);
    expect(LinkNodeUtils.isSpecialPage('someFunction(){return false;}', {}, currentHref, window)).toBe(false);
  });

  it('isSpecialPage should return true for external links in developer.mozilla.org', () => {
    const currentHref = 'https://developer.mozilla.org/en-US/docs/Tools/Debugger/Source_map_errors';
    const testNode = (id, expected) => {
      const node = document.getElementById(id);
      expect(LinkNodeUtils.isSpecialPage('', node, currentHref)).toBe(expected);
    };

    testNode('link1', true);
    testNode('link2', false);
    testNode('link3', false);

    // cover catch block in isSpecialPage
    expect(LinkNodeUtils.isSpecialPage()).toBe(false);
  });
});

describe('helpers functions', () => {
  it('getAttributes should return empty object', () => {
    const getAttributes = contentRewireAPI.__GetDependency__('getAttributes');
    expect(getAttributes()).toEqual({});
  });

  it('getAttributes should filter existing atrributes', () => {
    const getAttributes = contentRewireAPI.__GetDependency__('getAttributes');
    const node = document.getElementById('div_id');
    expect(getAttributes(node, [])).toEqual({});
    const expected = {'class': 'div_class', 'attr1': 'div_attr1', 'attr3': 'div_attr3'};
    expect(getAttributes(node, ['class', 'attr1', 'attr3'])).toEqual(expected);
  });

  it('getTargetAttr should return target from base', () => {
    const getTargetAttr = contentRewireAPI.__GetDependency__('getTargetAttr');

    expect(getTargetAttr('some_attribute', window)).toEqual('some_attribute');

    document.head.innerHTML = `
      <base href = "https://www.test_base.com" target="_blank"/>`;
    expect(getTargetAttr('', window)).toEqual('_blank');

    document.head.innerHTML = `
      <base href = "https://www.test_base.com"/>`;
    expect(getTargetAttr('', window)).toEqual(null);
    expect(getTargetAttr(undefined, window)).toEqual(null);
    expect(getTargetAttr(null, window)).toEqual(null);

    document.head.innerHTML = '';
    expect(getTargetAttr('', window)).toEqual('');
    expect(getTargetAttr(undefined, window)).toEqual(undefined);
    expect(getTargetAttr(null, window)).toEqual(null);
  });

  it('targetIsFrame should find if nested frame exist', () => {
    const targetIsFrame = contentRewireAPI.__GetDependency__('targetIsFrame');
    Array.from({length: 3}).reduce((dom, _, i) => {
      const frame = document.createElement('iframe');
      frame.name = `frame-name-${i}`;
      dom.appendChild(frame);
      return frame;
    }, document.body);
    expect(targetIsFrame('frame-name-0', window)).toBe(true);
    expect(targetIsFrame('frame-name-1', window)).toBe(true);
    expect(targetIsFrame('frame-name-2', window)).toBe(true);
    expect(targetIsFrame('frame-name-3', window)).toBe(false);
  });
});

describe('hrefAndLinkNodeForClickEvent', () => {
  const hrefAndLinkNodeForClickEvent = contentRewireAPI.__GetDependency__('hrefAndLinkNodeForClickEvent');
  const $ = id => document.getElementById(id);

  function testEvent(id) {
    return new Promise(resolve => {
      const onTest = event => {
        // we don't test nodePrincipal
        const [resultHref, resultNode] = hrefAndLinkNodeForClickEvent(event);
        resolve({href: resultHref, node: resultNode});
      };

      const node = $(id);
      window.addEventListener('test', onTest, true);
      node.dispatchEvent(new Event('test'));
      window.removeEventListener('test', onTest, true);
    });
  }
  it('should return href and linkNode for HTMLLink', async() => {
    expect(await testEvent('a_tag')).toEqual({href: 'https://wiki.a_tag.com/', node: $('a_tag')});
    expect(await testEvent('img')).toEqual({href: 'https://wiki.a_tag.com/', node: $('a_tag')});
    expect(await testEvent('area')).toEqual({href: 'https://wiki.area.com/', node: $('area')});
    expect(await testEvent('link')).toEqual({href: 'http://localhost/link.css', node: $('link')});
  });

  it('should return only href for xlink', async() => {
    expect(await testEvent('svg_text')).toEqual({href: 'https://www.w3schools.com/graphics/', node: null});
    expect(await testEvent('math')).toEqual({href: 'https://www.mathml.com/', node: null});
  });
});

describe('onMousedown', () => {
  const response = {where: 'tab'};
  const onMousedown = contentRewireAPI.__GetDependency__('onMousedown');
  beforeAll(() => {
    browser.runtime.sendMessage = jest.fn(() => response);
    window.history.pushState({}, 'Test Title', 'http://localhost/');
  });

  const $ = id => document.getElementById(id);

  function testEvent(id, options = {button: 0}) {
    return new Promise(resolve => {
      const onTest = async event => {
        await onMousedown(event);
        resolve();
      };

      const node = $(id);
      window.addEventListener('test-mousedown', onTest, true);
      const evt = new MouseEvent('test-mousedown', {
        button: options.button,
        bubbles: true,
        cancelable: true,
        view: window,
      });
      evt.originalTarget = node;
      evt.originalTarget.ownerDocument.nodePrincipal = {originAttributes: {}};
      if (options.tabmix_openLinkWithHistory) {
        evt.tabmix_openLinkWithHistory = true;
      }

      global.content = document.documentElement;
      node.dispatchEvent(evt);
      window.removeEventListener('test-mousedown', onTest, true);
    });
  }

  it('should call updateNodeProp button=0', () => {
    return testFunctions('updateNodeProp', async({updateNodeProp}) => {
      await testEvent('a_tag');
      expect(browser.runtime.sendMessage).toBeCalledTimes(1);
      expect(browser.runtime.sendMessage).toBeCalledWith(data.message);
      expect(updateNodeProp).toBeCalledTimes(1);
      expect(updateNodeProp).toBeCalledWith($('a_tag'), {where: 'tab'});
    });
  });

  it('should not call updateNodeProp', () => {
    response.where = 'default';
    return testFunctions('updateNodeProp', async({updateNodeProp}) => {
      await testEvent('a_tag');
      expect(browser.runtime.sendMessage).toBeCalledTimes(1);
      expect(browser.runtime.sendMessage).toBeCalledWith(data.message);
      expect(updateNodeProp).toBeCalledTimes(0);
    });
  });

  it('should call browser.runtime.sendMessage, button=1', () => {
    browser.runtime.sendMessage = jest.fn(() => {
      return {where: 'tab'};
    });
    return testFunctions('updateNodeProp', async() => {
      await testEvent('a_tag', {button: 1});
      const {event, href, node} = data.message;
      const event1 = {
        ...event,
        button: 1,
      };
      const node1 = {...node};
      delete node1.targetIsFrame;

      expect(browser.runtime.sendMessage).toBeCalledTimes(1);
      expect(browser.runtime.sendMessage).toBeCalledWith({event: event1, href, node: node1});
    });
  });

  it('should call browser.runtime.sendMessage, node with onclick', () => {
    return testFunctions('updateNodeProp', async() => {
      await testEvent('node_without_href');

      const {event, node} = data.message;
      const event1 = {
        ...event,
        originPrincipal: {originAttributes: {}},
        triggeringPrincipal: {originAttributes: {}},
      };
      const node1 = {
        ...node,
        _attributes: {onclick: 'parent_onclick_function();'},
        host: undefined, // no href
        pathname: undefined, // no href
        target: undefined, // div don't have a target attribute
      };
      delete node1.targetIsFrame;
      expect(browser.runtime.sendMessage).toBeCalledTimes(1);
      expect(browser.runtime.sendMessage).toBeCalledWith({event: event1, href: null, node: node1});
    });
  });

  it('should call browser.runtime.sendMessage, without href and node', () => {
    return testFunctions('updateNodeProp', async() => {
      await testEvent('deeper_node_without_href');
      const {event} = data.message;
      expect(browser.runtime.sendMessage).toBeCalledTimes(1);
      expect(browser.runtime.sendMessage).toBeCalledWith({event, href: null, node: undefined});
    });
  });

  it('should call browser.runtime.sendMessage, event with tabmix_openLinkWithHistory', () => {
    return testFunctions('updateNodeProp', async() => {
      await testEvent('deeper_node_without_href', {tabmix_openLinkWithHistory: true});
      const {event} = data.message;
      const event1 = {
        ...event,
        tabmix_openLinkWithHistory: true,
      };
      expect(browser.runtime.sendMessage).toBeCalledTimes(1);
      expect(browser.runtime.sendMessage).toBeCalledWith({event: event1, href: null, node: undefined});
    });
  });
});

describe('updateNodeProp', () => {
  // TODO - test it live on browser !!
  const updateNodeProp = contentRewireAPI.__GetDependency__('updateNodeProp');
  const cleanModifiedNode = contentRewireAPI.__GetDependency__('cleanModifiedNode');
  global.content = document.documentElement;

  const base = 'https://www.test_base.com';
  const bashHash = `${base}/#`;
  beforeAll(() => {
    document.head.innerHTML = `<base href = "${base}"/>`;
  });

  afterEach(() => {
    cleanModifiedNode();
  });

  function testUpdateNodeProp(node, response, extra) {
    node.setAttribute('onclick', 'onclick_function');
    if (extra.isLink) {
      node.setAttribute('target', '_self');
      node.setAttribute('href', 'https://wiki.mozilla.org');
    }

    updateNodeProp(node, response);
    expect(nodes.has(document.documentElement)).toBe(true);
    const nodeData = nodes.get(document.documentElement);

    const expectedData = {
      attributes: [
        ['onclick', 'onclick_function'],
        ['target', extra.isLink ? '_self' : null],
        ['href', extra.isLink ? 'https://wiki.mozilla.org' : null],
      ],
      href: 'http://tabmixplus.org',
      notLink: !extra.isLink,
      where: response.where,
    };

    const getAttr = (attr, def) => {
      return attr in extra ? extra[attr] : def;
    };

    expect(nodeData).toEqual({node, data: expectedData});
    expect(node.getAttribute('onclick')).toEqual(null);
    expect(node.getAttribute('target')).toEqual(getAttr('target', '_blank'));
    expect(node.href).toEqual(getAttr('href', 'http://tabmixplus.org/'));
  }

  it('should throw an error', () => {
    expect(() => {
      updateNodeProp(null);
    }).toThrowError('Tabmix:\nnode is undefined');

    const node = document.getElementById('link1');
    expect(() => {
      updateNodeProp(node, {where: 'somewhere'});
    }).toThrowError('Tabmix:\nunexpected response.where somewhere');
  });

  it('should add mouseout addEventListener to the node passed as argument', () => {
    const node = document.createElement('a');
    node.addEventListener = jest.fn();
    updateNodeProp(node, {where: 'tab', href: 'http://tabmixplus.org'});
    expect(node.addEventListener).toBeCalledTimes(1);
    const args = node.addEventListener.mock.calls[0];
    expect(args[0]).toEqual('mouseout');
    expect(typeof args[1]).toEqual('function');
    expect(args[2]).toEqual({capture: true, once: true});
  });

  it('should call resetNode by mouseout event on the node that passed as argument', () => {
    return testFunctions('resetNode', async({resetNode}) => {
      const node = document.createElement('a');
      updateNodeProp(node, {where: 'tab', href: 'http://tabmixplus.org'});
      await delayed(0);
      expect(resetNode).toBeCalledTimes(0);
      node.dispatchEvent(new Event('mouseout'));
      await delayed(0);
      expect(resetNode).toBeCalledTimes(1);
    });
  });

  it('should change "a" node to open new tab with hrefFromOnClick or href from response', () => {
    const response = {where: 'tab', href: 'http://tabmixplus.org'};
    testUpdateNodeProp(document.createElement('a'), response, {isLink: true});
    response.hrefFromOnClick = response.href;
    response.href = 'xxxxxxx';
    testUpdateNodeProp(document.createElement('a'), response, {isLink: true});
  });

  it('should change "a" node to open in current tab with hrefFromOnClick or href from response', () => {
    const response = {where: 'current', href: 'http://tabmixplus.org'};
    testUpdateNodeProp(document.createElement('a'), response, {isLink: true, href: bashHash, target: null});
    response.hrefFromOnClick = response.href;
    response.href = 'xxxxxxx';
    testUpdateNodeProp(document.createElement('a'), response, {isLink: true, href: bashHash, target: null});
  });

  it('should remove onclick attribute from non-link node', () => {
    const response = {where: 'tab', hrefFromOnClick: 'http://tabmixplus.org'};
    testUpdateNodeProp(document.createElement('div'), response, {isLink: false, target: null, href: undefined});
  });

  it('should update non-link node to open in current tab', () => {
    const response = {where: 'current', hrefFromOnClick: 'http://tabmixplus.org'};
    testUpdateNodeProp(document.createElement('div'), response, {isLink: false, target: null, href: '#'});
  });

  it('should update xlink node to open in current tab', () => {
    const node = document.getElementById('svg_text');
    const response = {where: 'current', hrefFromOnClick: 'http://tabmixplus.org'};
    testUpdateNodeProp(node, response, {isLink: false, target: null, href: '#'});

    const node1 = document.getElementById('math');
    testUpdateNodeProp(node1, response, {isLink: false, target: null, href: '#'});
  });
});

describe('onMouseClick', () => {
  const onMouseClick = contentRewireAPI.__GetDependency__('onMouseClick');
  const cleanModifiedNode = contentRewireAPI.__GetDependency__('cleanModifiedNode');

  window.open = jest.fn();
  global.content = document.documentElement;

  const event = {
    stopPropagation: jest.fn(),
    preventDefault: jest.fn(),
  };

  afterEach(() => {
    cleanModifiedNode();
    top.location.href = 'http://localhost/';
  });

  it('should not call resetNode when nodes map is empty', () => {
    return testFunctions('resetNode', async({resetNode}) => {
      onMouseClick(event);
      await delayed(0);
      expect(resetNode).toBeCalledTimes(0);
    });
  });

  it('should call resetNode when nodes map is not empty', () => {
    return testFunctions('resetNode', async({resetNode}) => {
      saveNodeData(document.createElement('a'), {notLink: false, where: 'not-current'});
      onMouseClick(event);
      await delayed(0);
      expect(resetNode).toBeCalledTimes(1);
    });
  });

  it('should not call stopPropagation and preventDefault', () => {
    saveNodeData(document.createElement('a'), {notLink: false, where: 'not-current'});
    onMouseClick(event);
    expect(event.stopPropagation).toBeCalledTimes(0);
    expect(event.preventDefault).toBeCalledTimes(0);
  });

  it('should call stopPropagation and preventDefault when node is not a link', () => {
    saveNodeData(document.createElement('a'), {notLink: true, where: 'not-current'});
    onMouseClick(event);
    expect(event.stopPropagation).toBeCalledTimes(1);
    expect(event.preventDefault).toBeCalledTimes(1);
  });

  it('should call stopPropagation and preventDefault when where is current', () => {
    saveNodeData(document.createElement('a'), {notLink: false, where: 'current'});
    onMouseClick(event);
    expect(event.stopPropagation).toBeCalledTimes(1);
    expect(event.preventDefault).toBeCalledTimes(1);
  });

  it('should not change location and not call window.open', () => {
    const location = top.location.href;
    saveNodeData(document.createElement('a'), {notLink: false, where: 'not-current'});
    onMouseClick(event);
    expect(top.location.href).toEqual(location);
    expect(window.open).toBeCalledTimes(0);
  });

  it('should change location when where is current', () => {
    const location = top.location.href;
    saveNodeData(document.createElement('a'), {notLink: false, where: 'current'});
    onMouseClick(event);
    expect(top.location.href).not.toEqual(location);
    expect(top.location.href).toEqual('http://localhost/#new_href');
    expect(window.open).toBeCalledTimes(0);
  });

  it('should call window.open when where is not current and node is not a link', () => {
    const location = top.location.href;
    saveNodeData(document.createElement('a'), {notLink: true, where: 'not-current'});
    onMouseClick(event);
    expect(top.location.href).toEqual(location);
    expect(window.open).toBeCalledTimes(1);
    expect(window.open).toBeCalledWith('http://localhost/#new_href', '_blank');
  });
});

describe('resetNode', () => {
  const cleanModifiedNode = contentRewireAPI.__GetDependency__('cleanModifiedNode');
  const originalResetNode = contentRewireAPI.__GetDependency__('resetNode');
  global.content = document.documentElement;

  afterEach(() => {
    nodes.delete(document.documentElement);
  });

  it('cleanModifiedNode should not call resetNode when nodes map is empty', () => {
    return testFunctions('resetNode', ({resetNode}) => {
      cleanModifiedNode();
      expect(resetNode).toBeCalledTimes(0);
    });
  });

  it('cleanModifiedNode should call resetNode when nodes map is not empty', () => {
    return testFunctions('resetNode', ({resetNode}) => {
      saveNodeData(document.createElement('a'), {notLink: false, where: 'not-current'});
      cleanModifiedNode();
      expect(resetNode).toBeCalledTimes(1);
    });
  });

  it('should not throw an error, when nodes is empty', () => {
    expect(() => {
      originalResetNode({});
    }).not.toThrowError('Tabmix:\ndata is undefined');
  });

  it('should throw an error', () => {
    saveNodeData(document.createElement('a'), {notLink: false, where: 'not-current'});
    expect(() => {
      originalResetNode({});
    }).toThrowError('Tabmix:\ndata is undefined');
  });

  it('should restore attributes', () => {
    const node = document.createElement('a');
    const savedData = saveNodeData(node);
    originalResetNode(node, savedData);
    expect(node.getAttribute('onclick')).toEqual('onclick_function');
    expect(node.getAttribute('target')).toEqual('_self');
    expect(node.getAttribute('href')).toEqual('https://wiki.mozilla.org');
  });

  it('should remove added attributes', () => {
    const node = document.createElement('a');
    const savedData = {
      attributes: [
        ['onclick', null],
        ['target', null],
        ['href', null],
      ],
    };
    nodes.set(document.documentElement, {node, data: savedData});

    // change original attributes
    node.setAttribute('onclick', 'some_function');
    node.setAttribute('target', 'some_target');
    node.setAttribute('href', 'some_href');

    originalResetNode(node, savedData);
    expect(node.getAttribute('onclick')).toEqual(null);
    expect(node.getAttribute('target')).toEqual(null);
    expect(node.getAttribute('href')).toEqual(null);
  });
});
