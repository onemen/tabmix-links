/**
* @jest-environment jsdom
*/
/* globals global */
import {getPreferences, prepareTabs, resetPreferences, rewireMethodFromHandler, rewireWith} from './_helpers.js';

let preferencesRewireAPI, testFunctions;
let preferences;
const $ = id => document.getElementById(id);

// read preferences.html
const fs = require('fs');
const html = fs.readFileSync('./addon/preferences/preferences.html', 'utf8');
const bodyHtml = /<body.*?>([\s\S]*)<\/body>/.exec(html)[1];
document.body.innerHTML = bodyHtml;

beforeAll(() => {
  preferences = getPreferences();
  global.preferences = preferences;
  preferencesRewireAPI = require('../addon/preferences/preferences.js');
  testFunctions = rewireWith.bind(null, preferencesRewireAPI);

  browser.tabs.getCurrent = jest.fn(() => {
    const activeTab = global.__tabs.filter(tab => tab.active);
    return Promise.resolve(activeTab.length ? activeTab[0] : global.__tabs[0]);
  });
});

afterAll(() => {
  resetPreferences();
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('prefWindow', () => {
  let prefWindow, rewireMethod;
  beforeAll(() => {
    prefWindow = preferencesRewireAPI.__GetDependency__('prefWindow');
    rewireMethod = rewireMethodFromHandler.bind(null, prefWindow);
  });

  it('initBroadcasters should call updateBroadcaster for each checkbox', () => {
    rewireMethod('updateBroadcaster', updateBroadcaster => {
      prefWindow.initBroadcasters();
      expect(updateBroadcaster).toBeCalledTimes(9);
    });
  });

  it('updateBroadcaster should NOT call setDisabled when item is not a checkbox with an id', () => {
    rewireMethod('setDisabled', setDisabled => {
      const item = document.createElement('input');
      item.setAttribute('type', 'checkbox');
      prefWindow.updateBroadcaster(item);
      expect(setDisabled).toBeCalledTimes(0);
    });

    rewireMethod('setDisabled', setDisabled => {
      const item = document.createElement('input');
      item.setAttribute('id', 'test-item');
      prefWindow.updateBroadcaster(item);
      expect(setDisabled).toBeCalledTimes(0);
    });
  });

  it('updateBroadcaster should call setDisabled when item is a checkbox with an id', () => {
    function updateBroadcaster(checked, inverseDependency) {
      const item = document.createElement('input');
      item.setAttribute('type', 'checkbox');
      item.setAttribute('id', 'test-item');
      item.checked = checked;
      if (inverseDependency) {
        item.setAttribute('inverseDependency', 'true');
      }
      prefWindow.updateBroadcaster(item);
    }

    rewireMethod('setDisabled', setDisabled => {
      updateBroadcaster(false, false);
      expect(setDisabled).toBeCalledTimes(1);
      expect(setDisabled).toBeCalledWith('obs_test-item', true);
    });

    rewireMethod('setDisabled', setDisabled => {
      updateBroadcaster(true, false);
      expect(setDisabled).toBeCalledTimes(1);
      expect(setDisabled).toBeCalledWith('obs_test-item', false);
    });

    rewireMethod('setDisabled', setDisabled => {
      updateBroadcaster(true, true);
      expect(setDisabled).toBeCalledTimes(1);
      expect(setDisabled).toBeCalledWith('obs_test-item', true);
    });
  });

  it('setDisabled should update disabled attribute for an item', () => {
    function setDisabled(val, inverseDependency) {
      const item = document.createElement('label');
      item.setAttribute('observes', 'obs_test-item');
      if (inverseDependency) {
        item.setAttribute('inverseDependency', 'true');
      }
      prefWindow.setDisabled(item, val);
      return item;
    }
    expect(setDisabled(true, false).getAttribute('disabled')).toBe('true');
    expect(setDisabled(false, true).getAttribute('disabled')).toBe('true');
    expect(setDisabled(true, true).hasAttribute('disabled')).toBe(false);
    expect(setDisabled(false, false).hasAttribute('disabled')).toBe(false);
  });

  function testDisabled(setDisabled) {
    const msg = `setDisabled should ${setDisabled ? 'remove' : 'add'} disabled attribute for group of items`;
    describe(msg, () => {
      beforeAll(() => {
        prefWindow.setDisabled('obs_opentabforLinks', setDisabled);
      });

      let counter = 1;
      const val = setDisabled ? 'true' : null;
      function addTest(item) {
        const name = item.id || `${item.nodeName.toLowerCase()}_${item.getAttribute('for') || item.childNodes[item.childNodes.length - 2].getAttribute('for')}`;
        it(`${counter++} setDisabled should update disabled attribute for: ${name}`, () => {
          const inverse = item.getAttribute('inverseDependency');
          expect(item.hasAttribute('disabled')).toBe(inverse ? !setDisabled : setDisabled);
          expect(item.getAttribute('disabled')).toBe(inverse ? val ? null : 'true' : val);
        });
      }

      const items = document.querySelectorAll('[observes="obs_opentabforLinks"]');
      for (const item of items) {
        addTest(item);
      }
    });
  }

  testDisabled(true);
  testDisabled(false);
});

describe('linksPane', () => {
  let linksPane, rewireMethod;
  beforeAll(() => {
    linksPane = preferencesRewireAPI.__GetDependency__('linksPane');
    rewireMethod = rewireMethodFromHandler.bind(null, linksPane);
  });

  it('init should call updateExternalLinkCheckBox', () => {
    rewireMethod('updateExternalLinkCheckBox', updateExternalLinkCheckBox => {
      linksPane.init();
      expect(updateExternalLinkCheckBox).toBeCalledTimes(1);
    });
  });

  it('updateExternalLinkCheckBox should update externalLink checked attribute', () => {
    function changeSelectedOption(val) {
      $('externalLinkTarget').value = val;
      linksPane.updateExternalLinkCheckBox();
      return $('externalLink').checked;
    }
    // valid options are -1, 1, 2, 3
    expect(changeSelectedOption(3)).toBe(true);
    expect(changeSelectedOption(2)).toBe(true);
    expect(changeSelectedOption(-1)).toBe(false);
    expect(changeSelectedOption(1)).toBe(true);
  });

  it('externalLinkValue should set all externalLinkTarget state', () => {
    const externalLinkTarget = $('externalLinkTarget');
    const generalWindowOpen = $('generalWindowOpen');
    const externalDefault = $('externalLinkTarget.disabled');
    const syncedItem = document.querySelector('[synced-item="open_newwindow.override.external"]');

    generalWindowOpen.value = 3;
    externalLinkTarget.value = 3;
    externalLinkTarget.disable = false;
    externalDefault.textContent = 'xxxx';
    externalDefault.hidden = true;
    syncedItem.textContent = 3;

    linksPane.externalLinkValue(false);
    expect(externalLinkTarget.value).toBe('-1');
    expect(externalLinkTarget.disabled).toBe(true);
    expect(externalDefault.textContent).toBe('New Tab');
    expect(externalDefault.hidden).toBe(false);
    expect(syncedItem.textContent).toBe('-1');

    generalWindowOpen.value = 2;
    linksPane.externalLinkValue(true);
    expect(externalLinkTarget.value).toBe('2');
    expect(externalLinkTarget.disabled).toBe(false);
    expect(externalDefault.textContent).toBe('New Window');
    expect(externalDefault.hidden).toBe(true);
    expect(syncedItem.textContent).toBe('2');
  });

  it('singleWindow should update related items state', () => {
    const externalLinkTarget = $('externalLinkTarget');
    const generalWindowOpen = $('generalWindowOpen');
    const divertedWindowOpen = $('divertedWindowOpen');
    const externalDefault = $('externalLinkTarget.disabled');
    const externalSyncedItem = document.querySelector('[synced-item="open_newwindow.override.external"]');
    const generalSyncedItem = document.querySelector('[synced-item="open_newwindow"]');
    const divertedSyncedItem = document.querySelector('[synced-item="open_newwindow.restriction"]');

    generalWindowOpen.value = '2';
    externalLinkTarget.value = '2';
    divertedWindowOpen.value = '1';
    externalSyncedItem.textContent = '2';
    generalSyncedItem.textContent = '2';
    divertedSyncedItem.textContent = '1';
    externalDefault.textContent = 'xxxx';

    linksPane.singleWindow(false);
    expect(generalWindowOpen.value).toBe('2');
    expect(externalLinkTarget.value).toBe('2');
    expect(divertedWindowOpen.value).toBe('1');
    expect(externalSyncedItem.textContent).toBe('2');
    expect(generalSyncedItem.textContent).toBe('2');
    expect(divertedSyncedItem.textContent).toBe('1');
    expect(externalDefault.textContent).toBe('xxxx');

    linksPane.singleWindow(true);
    expect(generalWindowOpen.value).toBe('3');
    expect(externalLinkTarget.value).toBe('3');
    expect(divertedWindowOpen.value).toBe('0');
    expect(externalSyncedItem.textContent).toBe('3');
    expect(generalSyncedItem.textContent).toBe('3');
    expect(divertedSyncedItem.textContent).toBe('0');
    expect(externalDefault.textContent).toBe('New Tab');
  });

  it('setMiddleCurrentDisabled should update related items disabled state', () => {
    const opentabforLinks = $('opentabforLinks');
    const middlecurrent = $('middlecurrent');
    const radiogroup = $('opentabforLinks.radiogroup');

    // init state
    middlecurrent.removeAttribute('disabled');
    middlecurrent.parentNode.removeAttribute('disabled');
    radiogroup.value = '1';

    opentabforLinks.checked = true;
    linksPane.setMiddleCurrentDisabled('2');
    expect(radiogroup.value).toBe('2');
    expect(middlecurrent.getAttribute('disabled')).toBe('true');
    expect(middlecurrent.parentNode.getAttribute('disabled')).toBe('true');

    opentabforLinks.checked = true;
    linksPane.setMiddleCurrentDisabled('1');
    expect(radiogroup.value).toBe('1');
    expect(middlecurrent.hasAttribute('disabled')).toBe(false);
    expect(middlecurrent.parentNode.hasAttribute('disabled')).toBe(false);

    opentabforLinks.checked = false;
    linksPane.setMiddleCurrentDisabled('1');
    expect(radiogroup.value).toBe('1');
    expect(middlecurrent.getAttribute('disabled')).toBe('true');
    expect(middlecurrent.parentNode.getAttribute('disabled')).toBe('true');

    opentabforLinks.checked = false;
    linksPane.setMiddleCurrentDisabled('2');
    expect(radiogroup.value).toBe('2');
    expect(middlecurrent.getAttribute('disabled')).toBe('true');
    expect(middlecurrent.parentNode.getAttribute('disabled')).toBe('true');
  });
});

describe('handleButtonClick', () => {
  let handleButtonClick;
  beforeAll(() => {
    handleButtonClick = preferencesRewireAPI.__GetDependency__('handleButtonClick');
    global.filetypeModal = {open: jest.fn()};
  });

  const functionList = [
    'openHelp',
    'openContributeTab',
    'defaultPreferences',
    'savePreferences',
    'closeTab',
    'copyToClipboard',
  ];

  function runTest(target, expected) {
    return testFunctions(functionList, rewired => {
      handleButtonClick({target});
      expect(rewired.openHelp).toBeCalledTimes(expected.openHelp || 0);
      expect(rewired.openContributeTab).toBeCalledTimes(expected.openContributeTab || 0);
      expect(global.filetypeModal.open).toBeCalledTimes(expected.filetypeModal_open || 0);
      expect(rewired.defaultPreferences).toBeCalledTimes(expected.defaultPreferences || 0);
      expect(rewired.savePreferences).toBeCalledTimes(expected.savePreferences || 0);
      expect(rewired.closeTab).toBeCalledTimes(expected.closeTab || 0);
      expect(rewired.copyToClipboard).toBeCalledTimes(expected.copyToClipboard || 0);
      return rewired;
    });
  }

  it('should call openHelp', () => {
    const target = document.createElement('button');
    target.setAttribute('helpTopic', 'test_helpTopic');
    const result = runTest(target, {openHelp: 1});
    expect(result.openHelp).toBeCalledWith('test_helpTopic');
  });

  it('should call openContributeTab', () => {
    const target = document.createElement('button');
    target.id = 'contribute-button';
    runTest(target, {openContributeTab: 1});
  });

  it('should call filetypeModal.open', () => {
    const target = document.createElement('button');
    target.id = 'filetype-edit-button';
    runTest(target, {filetypeModal_open: 1});
  });

  it('should call defaultPreferences', () => {
    const target = document.createElement('button');
    target.id = 'restore-default-button';
    runTest(target, {defaultPreferences: 1});
  });

  it('should call savePreferences and closeTab', () => {
    const target = document.createElement('button');
    target.id = 'accept-button';
    runTest(target, {savePreferences: 1, closeTab: 1});
  });

  it('should call savePreferences', () => {
    const target = document.createElement('button');
    target.id = 'apply-button';
    runTest(target, {savePreferences: 1});
  });

  it('should call closeTab', () => {
    const target = document.createElement('button');
    target.id = 'cancel-button';
    runTest(target, {closeTab: 1});
  });

  it('should call copyToClipboard', () => {
    const target = document.createElement('button');
    target.className = 'copy';
    const result = runTest(target, {copyToClipboard: 1});
    expect(result.copyToClipboard).toBeCalledWith(target);
  });
});

describe('preferences.js', () => {
  const helpPage = 'http://tabmixplus.org/support/viewtopic.php?t=3&p=';
  const tabsWithHelpUrl = ['url-1', 'url-2', `${helpPage}3`, 'url-4', 2];

  it('openContributeTab should open contribute.html in a tab after the current tab', async() => {
    const openContributeTab = preferencesRewireAPI.__GetDependency__('openContributeTab');
    prepareTabs(4, 2);
    await openContributeTab();
    expect(browser.tabs.query).toBeCalledTimes(1);
    expect(browser.tabs.create).toBeCalledTimes(1);
    const expected = {
      url: 'http://tabmixplus.org/support/contribute/contribute.html',
      index: 3,
    };
    expect(browser.tabs.create).toBeCalledWith(expected);
  });

  it('getOpenedHelpPage should return tab id from tab that match help url or null', async() => {
    const getOpenedHelpPage = preferencesRewireAPI.__GetDependency__('getOpenedHelpPage');
    prepareTabs(tabsWithHelpUrl);
    const result1 = await getOpenedHelpPage();
    expect(result1).toBe(3);

    prepareTabs(4);
    const result2 = await getOpenedHelpPage();
    expect(result2).toBe(null);
  });

  it('openHelp should select and reload tab with url that match help url', async() => {
    const openHelp = preferencesRewireAPI.__GetDependency__('openHelp');
    const tabs = tabsWithHelpUrl.slice();
    tabs.shift();

    prepareTabs(tabs);
    await openHelp('_helpTopic_');
    expect(browser.tabs.update).toBeCalledWith(2, {active: true, url: `${helpPage}_helpTopic_`});
    expect(browser.tabs.reload).toBeCalledWith(2);

    prepareTabs(tabsWithHelpUrl);
    await openHelp('_');
    expect(browser.tabs.update).toBeCalledWith(3, {active: true, url: `${helpPage}links`});
  });

  it('openHelp should create and select new tab, after the current tab, with url that match help url', async() => {
    const openHelp = preferencesRewireAPI.__GetDependency__('openHelp');
    global.__tabs = prepareTabs(4, 2);
    await openHelp('_help_page_');

    // test out mock function
    const current = await browser.tabs.getCurrent.mock.results[0].value;
    expect(current).toEqual({
      active: true,
      id: 3,
      index: 2,
      title: 'title-3',
      url: 'url-3',
    });

    expect(browser.tabs.create).toBeCalledWith({
      active: true,
      index: 3,
      url: `${helpPage}_help_page_`,
    });
  });

  it('onChange should call updatePrefItem and savePreferences', () => {
    const onChange = preferencesRewireAPI.__GetDependency__('onChange');

    testFunctions(['updatePrefItem', 'savePreferences'], ({updatePrefItem, savePreferences}) => {
      const target = document.createElement('input');
      target.setAttribute('linked', 'externalLinkTarget');
      onChange({target});
      expect(updatePrefItem).toBeCalledWith(target);
      expect(savePreferences).toBeCalledWith([$('externalLinkTarget')]);
    });

    testFunctions(['updatePrefItem', 'savePreferences'], ({updatePrefItem, savePreferences}) => {
      const target = document.createElement('input');
      onChange({target});
      expect(updatePrefItem).toBeCalledWith(target);
      expect(savePreferences).toBeCalledWith([target]);
    });
  });

  describe('updatePrefItem', () => {
    let updatePrefItem, prefWindow;
    beforeAll(() => {
      updatePrefItem = preferencesRewireAPI.__GetDependency__('updatePrefItem');
      prefWindow = preferencesRewireAPI.__GetDependency__('prefWindow');
    });

    it('should always call prefWindow.updateBroadcaster and updateSyncedValue', () => {
      const spy = jest.spyOn(prefWindow, 'updateBroadcaster');
      testFunctions('updateSyncedValue', ({updateSyncedValue}) => {
        const item = document.createElement('input');
        updatePrefItem(item);
        expect(prefWindow.updateBroadcaster).toBeCalledWith(item);
        expect(updateSyncedValue).toBeCalledWith(item);

        item.id = 'xxxxx';
        updatePrefItem(item);
        expect(prefWindow.updateBroadcaster).toBeCalledWith(item);
        expect(updateSyncedValue).toBeCalledWith(item);
      });
      spy.mockRestore();
    });

    it('should call linksPane.setMiddleCurrentDisabled after prefWindow.updateBroadcaster', () => {
      const spy = jest.spyOn(prefWindow, 'updateBroadcaster');
      const item = document.createElement('input');
      item.id = 'opentabforLinks';
      $('opentabforLinks.radiogroup').value = 2;

      const mockLinksPane = {
        name: 'linksPane',
        fn: {
          setMiddleCurrentDisabled: jest.fn(() => {
            expect(prefWindow.updateBroadcaster).toBeCalledTimes(1);
            expect(prefWindow.updateBroadcaster).toBeCalledWith(item);
          }),
        },
      };

      testFunctions(mockLinksPane, ({linksPane}) => {
        updatePrefItem(item);
        expect(linksPane.setMiddleCurrentDisabled).toBeCalledWith(2);
      });
      spy.mockRestore();
    });

    it('should call linksPane functions according to the item.id and before prefWindow.updateBroadcaster', () => {
      const originalLinksPane = preferencesRewireAPI.__GetDependency__('linksPane');
      const linksPaneSpy = jest.fn();
      const methods = Object.keys(originalLinksPane).reduce((obj, name) => {
        obj[name] = jest.fn(() => linksPaneSpy());
        return obj;
      }, {});
      const mockLinksPane = {
        name: 'linksPane',
        fn: methods,
      };

      const mockPrefWindow = {
        name: 'prefWindow',
        fn: {
          updateBroadcaster: jest.fn(item => {
            // verify that we call updateBroadcaster after linksPane methods
            expect(linksPaneSpy).toBeCalledTimes(item.id == 'xxxxx' ? 0 : 1);
          }),
        },
      };

      function runTest(item, mockedObj, tests, expected) {
        updatePrefItem(item);
        tests();
        Object.entries(mockedObj).forEach(([key, fn]) => {
          expect(fn).toBeCalledTimes(expected[key] || 0);
          fn.mockClear();
        });
        linksPaneSpy.mockClear();
      }

      testFunctions([mockLinksPane, mockPrefWindow], ({linksPane}) => {
        const item = document.createElement('input');
        item.id = 'singleWindow';
        item.checked = false;
        runTest(item, linksPane, () => {
          expect(linksPane.singleWindow).toBeCalledWith(false);
        }, {singleWindow: 1});

        item.checked = true;
        runTest(item, linksPane, () => {
          expect(linksPane.singleWindow).toBeCalledWith(true);
        }, {singleWindow: 1});

        item.id = 'generalWindowOpen';
        runTest(item, linksPane, () => {
          expect(linksPane.externalLinkLabel).toBeCalledWith(item);
        }, {externalLinkLabel: 1});

        item.id = 'externalLink';
        runTest(item, linksPane, () => {
          expect(linksPane.externalLinkValue).toBeCalledWith(true);
        }, {externalLinkValue: 1});

        item.id = 'openintab-all-links';
        item.setAttribute('value', 42);
        runTest(item, linksPane, () => {
          expect(linksPane.setMiddleCurrentDisabled).toBeCalledWith('42');
        }, {setMiddleCurrentDisabled: 1});

        item.id = 'openintab-other-sites';
        runTest(item, linksPane, () => {
          expect(linksPane.setMiddleCurrentDisabled).toBeCalledWith('42');
        }, {setMiddleCurrentDisabled: 1});

        item.id = 'xxxxx';
        runTest(item, linksPane, () => {}, {});
      });
    });
  });

  it('updateSyncedValue should update synced item textContent', () => {
    const updateSyncedValue = preferencesRewireAPI.__GetDependency__('updateSyncedValue');
    const item = document.createElement('input');
    item.setAttribute('value', 'input_value');
    const syncedItem = document.createElement('label');
    syncedItem.setAttribute('synced-item', 'preference_name');
    syncedItem.textContent = 'xxxxx';
    document.body.appendChild(syncedItem);
    updateSyncedValue(item);
    expect(syncedItem.textContent).toBe('xxxxx');

    item.setAttribute('preference', 'preference_name');
    updateSyncedValue(item);
    expect(syncedItem.textContent).toBe('input_value');

    item.setAttribute('type', 'checkbox');
    item.checked = false;
    updateSyncedValue(item);
    expect(syncedItem.textContent).toBe('false');

    item.checked = true;
    updateSyncedValue(item);
    expect(syncedItem.textContent).toBe('true');
  });

  describe('save with', () => {
    let originalConsoleError;
    beforeAll(() => {
      originalConsoleError = global.console.error;
      global.console.error = jest.fn();
    });

    afterAll(() => {
      global.console.error = originalConsoleError;
    });

    const expectedError = new Error('test error');

    it('defaultPreferences should update call updateUI and save default preferences', () => {
      const defaultPreferences = preferencesRewireAPI.__GetDependency__('defaultPreferences');
      const prefs = {
        pref_1: 1,
        pref_2: 2,
        pref_3: '3',
        pref_4: true,
        pref_5: false,
      };

      const keys = Object.keys(prefs);
      global.defaultValues = prefs;

      const expectedErrorMsg = 'Tabmix: Error occurred when trying to save default preferences\n';
      browser.storage.local.set = jest.fn(() => {
        return Promise.reject(expectedError);
      });

      return testFunctions('updateUI', async({updateUI}) => {
        await defaultPreferences();
        expect(updateUI).toBeCalledTimes(1);
        expect(updateUI).toBeCalledWith(keys);
        expect(browser.storage.local.set).toBeCalledTimes(1);
        expect(browser.storage.local.set).toBeCalledWith(prefs);
        expect(global.console.error).toBeCalledTimes(1);
        expect(global.console.error).toBeCalledWith(expectedErrorMsg, expectedError);
      });
    });

    it('savePreferences should log errors', () => {
      const savePreferences = preferencesRewireAPI.__GetDependency__('savePreferences');
      const item = document.createElement('input');
      item.setAttribute('type', 'some_type');
      savePreferences([item]);
      expect(global.console.error).toBeCalledTimes(1);
      expect(global.console.error).toBeCalledWith('Tabmix Error: unknown item type', item.getAttribute('type'), item);
    });

    it('savePreferences should call browser.storage.local.set for change preferences', async() => {
      const savePreferences = preferencesRewireAPI.__GetDependency__('savePreferences');

      const expectedErrorMsg = 'Tabmix: Error occurred when trying to save preferences\n';
      browser.storage.local.set = jest.fn(() => {
        return Promise.reject(expectedError);
      });

      const item1 = $('generalWindowOpen');
      item1.value = 1;
      const item2 = $('opentabfor.middleclick');
      item2.checked = false;
      const item3 = $('opentabforLinks.radiogroup');
      $('opentabforLinks').checked = true;
      // this event will trigger savePreferences so we set call counter to 2
      $('openintab-other-sites').click();

      const update = {
        'open_newwindow': 1,
        'browser.tabs.opentabfor.middleclick': false,
        'opentabforLinks': 2,
      };

      await savePreferences([item1, item2, item3]);
      expect(browser.storage.local.set).toBeCalledTimes(2);
      expect(browser.storage.local.set).lastCalledWith(update);
      expect(global.console.error).toBeCalledTimes(2);
      expect(global.console.error).toBeCalledWith(expectedErrorMsg, expectedError);

      $('openintab-all-links').click();
      update.opentabforLinks = 1;
      await savePreferences([item1, item2, item3]);
      expect(browser.storage.local.set).lastCalledWith(update);

      $('opentabforLinks').click();
      update.opentabforLinks = 0;
      await savePreferences([item1, item2, item3]);
      expect(browser.storage.local.set).lastCalledWith(update);
    });

    it('savePreferences should call browser.storage.local.set with all preferences', async() => {
      const savePreferences = preferencesRewireAPI.__GetDependency__('savePreferences');

      // restore default
      global.defaultValues = preferences;
      $('restore-default-button').click();

      // filetype is not save with savePreferences
      const prefs = {...preferences};
      delete prefs.filetype;

      await savePreferences();
      expect(browser.storage.local.set).toBeCalledWith(prefs);
    });
  });

  it('closeTab should close current tab', async() => {
    const closeTab = preferencesRewireAPI.__GetDependency__('closeTab');
    global.__tabs = prepareTabs(3, 1);
    await closeTab();
    expect(browser.tabs.getCurrent).toBeCalledTimes(1);
    expect(browser.tabs.remove).toBeCalledTimes(1);
    expect(browser.tabs.remove).toBeCalledWith(2);
  });

  // TODO: find a way to test copyToClipboard
  /*
  it('copyToClipboard should preference name to the clipboard', () => {
    const copyToClipboard = preferencesRewireAPI.__GetDependency__('copyToClipboard');
    const buttons = document.querySelectorAll('button[class="copy"]');
    buttons.forEach(button => {
      copyToClipboard(button);
    });
  });
  */

  it('updateUI should update node for all preferences that passed as arguments', () => {
    const updateUI = preferencesRewireAPI.__GetDependency__('updateUI');
    const linksPane = preferencesRewireAPI.__GetDependency__('linksPane');
    const prefWindow = preferencesRewireAPI.__GetDependency__('prefWindow');

    jest.spyOn(linksPane, 'init');
    jest.spyOn(prefWindow, 'initBroadcasters');
    jest.spyOn(linksPane, 'setMiddleCurrentDisabled');

    const divertedWindowOpen = $('divertedWindowOpen');
    divertedWindowOpen.value = '1';
    preferences['open_newwindow.restriction'] = 0;

    const targetIsFrame = $('targetIsFrame');
    targetIsFrame.checked = true;
    preferences.targetIsFrame = false;

    const radiogroup = $('opentabforLinks.radiogroup');
    const opentabforLinks = $('opentabforLinks');
    radiogroup.value = '2';
    opentabforLinks.checked = true;
    $('openintab-other-sites').checked = true;
    preferences.opentabforLinks = 0;

    const originalConsoleError = global.console.error;
    global.console.error = jest.fn();

    testFunctions('updatePrefItem', ({updatePrefItem}) => {
      updateUI(['xxxxx', 'open_newwindow.restriction', 'targetIsFrame', 'opentabforLinks']);

      expect(updatePrefItem).toBeCalledTimes(3);

      expect(updatePrefItem.mock.calls[0][0]).toEqual(divertedWindowOpen);
      expect(updatePrefItem.mock.calls[1][0]).toEqual(targetIsFrame);
      expect(updatePrefItem.mock.calls[2][0]).toEqual(radiogroup);

      expect(divertedWindowOpen.value).toBe('0');
      expect(targetIsFrame.checked).toBe(false);

      // the default value for the radio group is 1
      expect(radiogroup.value).toBe('1');
      expect(opentabforLinks.checked).toBe(false);
      expect($('openintab-all-links').checked).toBe(true);
      expect($('openintab-other-sites').checked).toBe(false);

      expect(linksPane.init).toBeCalledTimes(1);
      expect(prefWindow.initBroadcasters).toBeCalledTimes(1);
      expect(linksPane.setMiddleCurrentDisabled).toBeCalledTimes(1);
      expect(linksPane.setMiddleCurrentDisabled).toBeCalledWith($('opentabforLinks.radiogroup').value);
      expect(global.console.error).toBeCalledTimes(1);
      expect(global.console.error).toBeCalledWith('Tabmix Error: missing item for xxxxx preference');

      global.console.error = originalConsoleError;
    });
  });
});
