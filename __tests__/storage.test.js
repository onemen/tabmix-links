import {getPreferences, resetPreferences, rewireWith} from './_helpers.js';
import {__RewireAPI__ as storageRewireAPI} from '../addon/common/storage.js';

const testFunctions = rewireWith.bind(null, storageRewireAPI);

let preferences, defaultsPrefs;

beforeAll(() => {
  preferences = getPreferences();
  preferences.pref_number = 10;
  preferences.pref_boolean = false;

  const defaultValues = storageRewireAPI.__GetDependency__('defaultValues');
  defaultsPrefs = Object.keys(defaultValues);
  defaultValues.pref_number = 10;
  defaultValues.pref_boolean = false;
});

afterAll(() => {
  resetPreferences();
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('storage updatePref', () => {
  const updatePref = storageRewireAPI.__GetDependency__('updatePref');

  it('should return false', () => {
    // newValue undefined
    expect(updatePref('pref_number')).toBe(false);
    // newValue undefined
    expect(updatePref('pref_not_exist', 100)).toBe(false);
  });

  it('should return true', () => {
    expect(updatePref('pref_number', 100)).toBe(true);
    expect(updatePref('pref_number', true)).toBe(true);
    expect(updatePref('pref_boolean', false)).toBe(true);
    expect(updatePref('pref_boolean', 11)).toBe(true);
  });

  it('should update preference by initial type', () => {
    updatePref('pref_number', 100);
    expect(preferences.pref_number).toEqual(100);
    updatePref('pref_number', true);
    expect(preferences.pref_number).toEqual(1);
    updatePref('pref_number', false);
    expect(preferences.pref_number).toEqual(0);
    updatePref('pref_number', '-100');
    expect(preferences.pref_number).toEqual(-100);
    updatePref('pref_number', 'true');
    // NaN
    expect(preferences.pref_number).toEqual(Number('xxx'));

    updatePref('pref_boolean', 100);
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', -100);
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', '100');
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', true);
    expect(preferences.pref_boolean).toEqual(true);
    updatePref('pref_boolean', false);
    expect(preferences.pref_boolean).toEqual(false);
  });
});

describe('storage', () => {
  const onStorageChanged = storageRewireAPI.__GetDependency__('onStorageChanged');

  it('onStorageChanged should not call notifyListeners', () => {
    jest.spyOn(preferences.onChanged, 'notifyListeners');
    onStorageChanged([], 'area_not_local');
    expect(preferences.onChanged.notifyListeners).toBeCalledTimes(0);
  });

  it('listener should be called immediately when addListener called with ', async() => {
    const listener = jest.fn();
    await preferences.onChanged.addListener(listener, true);
    expect(listener).toBeCalledTimes(1);
    expect(listener).toBeCalledWith(defaultsPrefs);
    preferences.onChanged.listeners = [];
  });

  it('onStorageChanged should call notifyListeners', () => {
    const listener = jest.fn();
    preferences.onChanged.addListener(listener);
    const changes = {
      'enablefiletype': {oldValue: true, newValue: false},
      'open_newwindow.restriction': {oldValue: 2, newValue: 2},
      'pref_number': {oldValue: 10, newValue: 100},
      'pref_boolean': {oldValue: false, newValue: false},
    };

    jest.spyOn(preferences.onChanged, 'notifyListeners');
    const original = storageRewireAPI.__GetDependency__('updatePref');
    const mockUpdatePref = {name: 'updatePref', fn: jest.fn((...args) => original(...args))};
    return testFunctions([mockUpdatePref], ({updatePref}) => {
      onStorageChanged(changes, 'local');

      expect(updatePref).toBeCalledTimes(2);
      expect(updatePref).toBeCalledWith('enablefiletype', false);
      expect(updatePref).toBeCalledWith('pref_number', 100);

      expect(preferences.onChanged.notifyListeners).toBeCalledTimes(1);
      expect(preferences.onChanged.notifyListeners).toBeCalledWith(['enablefiletype', 'pref_number']);

      expect(listener).toBeCalledTimes(1);
      expect(listener).toBeCalledWith(['enablefiletype', 'pref_number']);
      preferences.onChanged.listeners = [];
    });
  });

  it('updateAllPrefs should call updatePref for each pref and return list', () => {
    const updateAllPrefs = storageRewireAPI.__GetDependency__('updateAllPrefs');

    const original = storageRewireAPI.__GetDependency__('updatePref');
    const mockUpdatePref = {name: 'updatePref', fn: jest.fn((...args) => original(...args))};
    return testFunctions([mockUpdatePref], ({updatePref}) => {
      const result = updateAllPrefs({
        'open_newwindow.restriction': 3,
        'browser.altClickSave': true,
        'browser.tabs.opentabfor.middleclick': false,
        'pref_boolean': true,
        'open_newwindow.override.external': 1,
      });

      expect(updatePref).toBeCalledTimes(5);
      expect(result).toEqual([
        'open_newwindow',
        'open_newwindow.override.external',
        'open_newwindow.restriction',
        'browser.altClickSave',
        'browser.tabs.opentabfor.middleclick',
        'opentabforLinks',
        'enablefiletype',
        'middlecurrent',
        'linkTarget',
        'targetIsFrame',
        'singleWindow',
        'filetype',
        'pref_number',
        'pref_boolean',
      ]);

      expect(preferences['open_newwindow.restriction']).toEqual(3);
      expect(preferences['browser.altClickSave']).toEqual(true);
      expect(preferences['browser.tabs.opentabfor.middleclick']).toEqual(false);
      expect(preferences.pref_boolean).toEqual(true);
      expect(preferences['open_newwindow.override.external']).toEqual(1);
    });
  });
});
