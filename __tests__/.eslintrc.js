module.exports = {
  "extends": [
    "plugin:jest/recommended",
  ],

  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module"
  },

  "rules": {
    "jest/consistent-test-it": "error"
  }
};
