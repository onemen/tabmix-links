import {__RewireAPI__ as storageRewireAPI} from '../addon/common/storage.js';
const preferences = storageRewireAPI.__GetDependency__('preferences');

let preferencesCopy;

// helpers functions

const deepClone = obj => JSON.parse(JSON.stringify(obj));

export const cloneObject = (obj, extra) => {
  let clone = deepClone(obj);
  if (extra) {
    clone = Object.assign(clone, extra);
  }
  return clone;
};

export function delayed(wait) {
  return new Promise(resolve => {
    setTimeout(resolve, wait);
  });
}

export const copyObject = (a, b) => {
  Object.entries(a).forEach(([key, value]) => {
    b[key] = value;
  });
};

export const getPreferences = () => {
  preferencesCopy = cloneObject(preferences);
  return preferences;
};

export const resetPreferences = () => {
  if (preferencesCopy) {
    copyObject(preferencesCopy, preferences);
  }
  return preferences;
};

// helper function to mock browser.tabs.query
export function prepareTabs(list, activeIndex = 0) {
  const isArray = Array.isArray(list);
  const n = isArray ? list.length : list;
  const tabs = Array.from({length: n}, (v, i) => ({
    id: i + 1,
    index: i,
    title: `title-${i + 1}`,
    url: isArray ? list[i] : `url-${i + 1}`,
  }));
  tabs[activeIndex].active = true;
  browser.tabs.query = jest.fn(({active}) => {
    if (active) {
      const activeTab = tabs.filter(tab => tab.active);
      return Promise.resolve(activeTab.length ? activeTab : [tabs[0]]);
    }
    return Promise.resolve(tabs);
  });
  return tabs;
}

function runCallbackAndReset(callback, reset, args) {
  let result;
  try {
    result = callback(args);
    if (Boolean(result) && typeof result.then == 'function') {
      result.then(reset).catch(reset);
    } else {
      reset();
    }
  } catch (ex) {
    reset();
    // forward the error
    throw ex;
  }

  return result;
}

export function rewireWith(rewireAPI, _names, callback) {
  const names = Array.isArray(_names) ? _names : [_names];
  // rewire and get rewired functions
  const rewiredFunctions = names.reduce((rewired, fnName) => {
    const {name = fnName, fn = jest.fn()} = fnName;
    rewireAPI.__Rewire__(name, fn);
    rewired[name] = rewireAPI.__GetDependency__(name);
    return rewired;
  }, {});

  function reset() {
    names.forEach(fnName => {
      const {name = fnName} = fnName;
      rewireAPI.__ResetDependency__(name);
    });
  }

  return runCallbackAndReset(callback, reset, rewiredFunctions);
}

export function mockMethods(handler, _names, callback) {
  const names = Array.isArray(_names) ? _names : [_names];
  // mock and get mocked methods
  const mockedMethods = names.reduce((mocked, method) => {
    const {name = method, mock = (() => true)} = method;
    const spy = jest.spyOn(handler, name);
    spy.mockImplementation(mock);
    mocked[name] = spy;
    return mocked;
  }, {});

  function reset() {
    Object.keys(mockedMethods).forEach(name => {
      handler[name].mockRestore();
    });
  }

  return runCallbackAndReset(callback, reset, mockedMethods);
}

export function rewireMethodFromHandler(handler, name, fn, method) {
  const mock = method || (() => true);
  const callback = () => fn(handler[name]);
  return mockMethods(handler, {name, mock}, callback);
}
