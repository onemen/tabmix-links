/* globals module */

function messageHelper() {
  this.listeners = [];
  const command = this.command.bind(this);
  this.command = jest.fn(command);
}

messageHelper.prototype = {
  listeners: [],
  command(message) {
    const cb = this.listeners[0];
    if (cb !== undefined) {
      return cb(message);
    }
    return Promise.resolve();
  },
  addListener: jest.fn(function(listener) {
    if (listener && !this.hasListener(listener)) {
      this.listeners.push(listener);
    }
  }),
  removeListener: jest.fn(function(listener) {
    const index = this.listeners.findIndex(item => item == listener);
    if (index > -1) {
      this.listeners.splice(index, 1);
    }
  }),
  hasListener: jest.fn(function(listener) {
    return this.listeners.includes(listener);
  }),
};

module.exports = messageHelper;
