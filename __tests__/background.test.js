/* globals global */
import {data} from './fixtures/data.js';
import {getPreferences, resetPreferences, rewireMethodFromHandler, rewireWith} from './_helpers.js';
import {__RewireAPI__ as backgroundRewireAPI} from '../addon/background_scripts/background.js';

const testFunctions = rewireWith.bind(null, backgroundRewireAPI);

let preferences;

beforeAll(() => {
  const loggerRewireAPI = require('../addon/common/logger.js');
  global.log = loggerRewireAPI.__GetDependency__('log');
  global.debug = loggerRewireAPI.__GetDependency__('debug');

  // initialize preferences
  preferences = getPreferences();
  global.preferences = preferences;

  global.getDomain = require('tldjs').getDomain;
});

beforeEach(() => {
  jest.clearAllMocks();
  preferences.enablefiletype = false;
});

afterEach(() => {
  resetPreferences();
});

describe('helper functions', () => {
  it('isNewTabUrls should return true for urls from the list', () => {
    const Tabmix = backgroundRewireAPI.__GetDependency__('Tabmix');
    const urlList = Tabmix.newTabUrls.reduce((list, url) => {
      list.push([url, true]);
      return list;
    }, []);
    urlList.push(['https://wiki.a_tag.com', false]);
    urlList.push(['http://tabmixplus.org', false]);
    for (const [url, val] of urlList) {
      expect(Tabmix.isNewTabUrls(url)).toEqual(val);
    }
  });

  it('whereToOpenLink should return expected value', () => {
    const ignoreButton = true;
    const ignoreAlt = true;
    const whereToOpenLink = backgroundRewireAPI.__GetDependency__('whereToOpenLink');

    expect(whereToOpenLink()).toEqual('current');

    backgroundRewireAPI.__Rewire__('platform', 'mac');
    expect(whereToOpenLink({metaKey: true})).toEqual('tab');
    expect(whereToOpenLink({metaKey: true, shiftKey: true})).toEqual('tabshifted');

    backgroundRewireAPI.__Rewire__('platform', 'win');
    expect(whereToOpenLink({ctrlKey: true})).toEqual('tab');
    expect(whereToOpenLink({ctrlKey: true, shiftKey: true})).toEqual('tabshifted');

    const middleclick = preferences['browser.tabs.opentabfor.middleclick'];
    preferences['browser.tabs.opentabfor.middleclick'] = true;
    expect(whereToOpenLink({button: 1}, ignoreButton)).toEqual('current');
    expect(whereToOpenLink({button: 1})).toEqual('tab');
    expect(whereToOpenLink({button: 1, shiftKey: true})).toEqual('tabshifted');
    preferences['browser.tabs.opentabfor.middleclick'] = false;
    expect(whereToOpenLink({button: 1}, ignoreButton)).toEqual('current');
    expect(whereToOpenLink({button: 1}, false)).toEqual('window');
    expect(whereToOpenLink({button: 1, shiftKey: true})).toEqual('window');
    preferences['browser.tabs.opentabfor.middleclick'] = middleclick;

    const altClickSave = preferences['browser.altClickSave'];
    preferences['browser.altClickSave'] = true;
    expect(whereToOpenLink({altKey: true})).toEqual('save');
    expect(whereToOpenLink({altKey: true}, false, ignoreAlt)).toEqual('current');
    preferences['browser.altClickSave'] = false;
    expect(whereToOpenLink({altKey: true})).toEqual('current');
    expect(whereToOpenLink({altKey: true}, false, ignoreAlt)).toEqual('current');
    preferences['browser.altClickSave'] = altClickSave;

    expect(whereToOpenLink({shiftKey: true})).toEqual('window');
    backgroundRewireAPI.__ResetDependency__('platform');
  });

  it('getBoolPref should return default value', () => {
    const getBoolPref = backgroundRewireAPI.__GetDependency__('getBoolPref');
    expect(getBoolPref('xxxxxx', 'default_value')).toEqual('default_value');
  });
});

describe('receiveMessage', () => {
  const linksHandler = backgroundRewireAPI.__GetDependency__('linksHandler');

  it('should return {where: \'default\'}', async() => {
    const {event, node} = data.messageJson;
    const result = await linksHandler.receiveMessage({event, node});
    expect(result).toEqual({where: 'default'});
  });

  it('should return object', async() => {
    const result = await linksHandler.receiveMessage(data.messageJson);
    const expected = {
      href: 'https://wiki.a_tag.com/',
      suppressTabsOnFileDownload: false,
      targetAttr: '',
      where: 'default',
    };
    expect(result).toEqual(expected);
  });

  it('should return object with hrefFromOnClick', async() => {
    const middleclick = preferences['browser.tabs.opentabfor.middleclick'];
    preferences['browser.tabs.opentabfor.middleclick'] = true;
    const {event, node} = data.messageJson;
    event.button = 1;
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    const result = await linksHandler.receiveMessage({event, node});
    const expected = {
      href: 'https://www.test_base.com/',
      hrefFromOnClick: 'https://www.test_base.com/',
      suppressTabsOnFileDownload: false,
      targetAttr: null,
      where: 'tab',
    };
    expect(result).toEqual(expected);
    preferences['browser.tabs.opentabfor.middleclick'] = middleclick;
  });
});

describe('whereToOpen', () => {
  const linksHandler = backgroundRewireAPI.__GetDependency__('linksHandler');

  beforeEach(() => {
    linksHandler._data = null;
  });

  const rewireMethod = rewireMethodFromHandler.bind(null, linksHandler);

  function whereToOpen({event, href, node, onClickNode}, expected) {
    const clonedEvent = {...event};
    let result;
    if (onClickNode) {
      const clonedNode = !onClickNode ? onClickNode : {...onClickNode};
      const wrappedNode = linksHandler.getWrappedNode(clonedNode);
      result = linksHandler.whereToOpen(clonedEvent, href, null, wrappedNode);
    } else {
      const clonedNode = !node ? node : {...node};
      const wrappedNode = linksHandler.getWrappedNode(clonedNode);
      result = linksHandler.whereToOpen(clonedEvent, href, wrappedNode);
    }
    expect(result).toEqual(expected);
  }

  // linksHandler._getParamsForLink and call linksHandler.receiveMessage
  function receiveMessage({event, href, node}, expected) {
    const _getParamsForLink = linksHandler._getParamsForLink;
    linksHandler._getParamsForLink = function(_event, wrappedNode, _href, wrappedOnClickNode) {
      const args = {event: _event, href: _href, node: wrappedNode, onClickNode: wrappedOnClickNode};
      whereToOpen(args, expected);
    };

    linksHandler.receiveMessage({event, href, node});
    linksHandler._getParamsForLink = _getParamsForLink;
  }

  it('getWrappedNode should add functions to json', () => {
    const {node} = data.messageJson;

    expect(node.hasAttribute).toBeUndefined();
    expect(node.getAttribute).toBeUndefined();
    expect(node.parentNode.hasAttribute).toBeUndefined();
    expect(node.parentNode.getAttribute).toBeUndefined();

    let wrappedNode = linksHandler.getWrappedNode();
    expect(wrappedNode).toEqual(null);
    wrappedNode = linksHandler.getWrappedNode(node);

    // test that we not changing the original object
    expect(node.hasAttribute).toBeUndefined();
    expect(node.getAttribute).toBeUndefined();
    expect(node.parentNode.hasAttribute).toBeUndefined();
    expect(node.parentNode.getAttribute).toBeUndefined();
    expect(typeof wrappedNode.hasAttribute).toEqual('function');
    expect(typeof wrappedNode.getAttribute).toEqual('function');
    expect(typeof wrappedNode.parentNode.hasAttribute).toEqual('function');
    expect(typeof wrappedNode.parentNode.getAttribute).toEqual('function');
  });

  it('should return default@2', () => {
    const {event, href} = data.messageJson;
    whereToOpen({event, href, node: null}, ['default@2']);
  });

  it('should return default@2.1', () => {
    //  whereToOpenLink return save or window
    testFunctions({name: 'whereToOpenLink', fn: () => 'save'}, () => {
      whereToOpen(data.messageJson, ['save@2.1']);
      expect(linksHandler._data.event.__hrefFromOnClick).toEqual(null);
    });

    testFunctions({name: 'whereToOpenLink', fn: () => 'window'}, () => {
      whereToOpen(data.messageJson, ['window@2.1']);
    });
  });

  it('should return default@2.1 and set __hrefFromOnClick', () => {
    //  whereToOpenLink return save or window
    testFunctions({name: 'whereToOpenLink', fn: () => 'save'}, () => {
      const {event, node} = data.messageJson;
      node._attributes.onclick = 'top.location.href=https://www.test_base.com';
      whereToOpen({event, href: '', node}, ['save@2.1']);
      expect(linksHandler._data.event.__hrefFromOnClick).toEqual('https://www.test_base.com/');
    });

    testFunctions({name: 'whereToOpenLink', fn: () => 'window'}, () => {
      const {event, node} = data.messageJson;
      node._attributes.onclick = 'top.location.href=https://www.test_base.com';
      whereToOpen({event, href: '', node}, ['window@2.1']);
      expect(linksHandler._data.event.__hrefFromOnClick).toEqual('https://www.test_base.com/');
    });
  });

  it('should return default@2.2', () => {
    rewireMethod('miscellaneous', () => {
      whereToOpen(data.messageJson, ['default@2.2']);
    });

    const {event, href, node} = data.messageJson;
    const clonedNode = {...node};
    // clonedNode.className = 'some_class __noscriptPlaceholder__';
    clonedNode.className = 'some_class';
    whereToOpen({event, href, node: clonedNode}, ['default@17']);

    clonedNode.className = 'some_class __noscriptPlaceholder__';
    whereToOpen({event, href, node: clonedNode}, ['default@2.2']);

    clonedNode.className = 'some_class button';
    whereToOpen({event, href, node: clonedNode}, ['default@2.2']);

    clonedNode.className = 'flag-review';
    event._currentURL = 'https://addons.mozilla.org';
    whereToOpen({event, href, node: clonedNode}, ['default@2.2']);

    delete clonedNode.className;
    event._currentURL = 'https://addons.mozilla.org';
    whereToOpen({event, href, node: clonedNode}, ['default@17']);

    node._attributes.role = 'button';
    whereToOpen({event, href, node: clonedNode}, ['default@2.2']);

    node._attributes.role = 'menu';
    whereToOpen({event, href, node: clonedNode}, ['default@2.2']);

    node._attributes.role = 'xxx-menu';
    whereToOpen({event, href, node: clonedNode}, ['default@17']);

    const Tabmix = backgroundRewireAPI.__GetDependency__('Tabmix');
    const isNewTabUrls = Tabmix.isNewTabUrls;
    Tabmix.isNewTabUrls = jest.fn(() => true);
    whereToOpen(data.messageJson, ['default@2.2']);
    Tabmix.isNewTabUrls = isNewTabUrls;
  });

  it('should return default@3', () => {
    rewireMethod('isGreasemonkeyScript', () => {
      whereToOpen(data.messageJson, ['default@3']);
    });
  });

  it('should return default@4', () => {
    // TODO test how the code work for link in iframe with onclick
    // XXX check if i can use activeYab.utl instead of _focusedWindowHref
    const {event, href, node} = data.messageJson;
    const wrappedNode = linksHandler.getWrappedNode(node);
    wrappedNode._focusedWindowHref = 'new_location';
    const result1 = linksHandler.whereToOpen(event, href, wrappedNode);
    expect(result1).toEqual(['default@17']);
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    const result2 = linksHandler.whereToOpen(event, href, wrappedNode);
    expect(result2).toEqual(['default@4']);
  });

  it('should return default@5', () => {
    const {event, href, node} = data.messageJson;
    node.target = '_search';
    whereToOpen({event, href, node}, ['default@5']);

    node.target = '';
    node._attributes.rel = 'sidebar';
    whereToOpen({event, href, node}, ['default@5']);

    delete node._attributes.rel;
    const mailtoHref = 'xxxx_mailto:____';
    whereToOpen({event, href: mailtoHref, node}, ['default@5']);
  });

  it('should return default@6 or current@7', () => {
    rewireMethod('suppressTabsOnFileDownload', () => {
      const {event, href, node} = data.messageJson;
      whereToOpen({event, href, node}, ['default@6', true]);

      event._pinned = true;
      whereToOpen({event, href, node}, ['current@7', true]);

      event._pinned = false;
      const notHttps = 'http://wiki.a_tag.com/';
      whereToOpen({event, href: notHttps, node}, ['current@7', true]);

      event._currentURL = 'https://mail.google.com';
      whereToOpen({event, href: notHttps, node}, ['default@6', true]);
    });
  });

  it('should return current@9', () => {
    const {event, href, node} = data.messageJson;
    event.tabmix_openLinkWithHistory = true;
    whereToOpen({event, href, node}, ['current@9']);
  });

  describe('with divertMiddleClick true', () => {
    it('should return current@10', () => {
      rewireMethod('divertMiddleClick', () => {
        const {event, href, node} = data.messageJson;
        node._attributes.onclick = 'top.location.href=https://www.test_base.com';
        whereToOpen({event, href, node}, ['current@10']);
        expect(linksHandler._data.event.__hrefFromOnClick).toEqual(null);
      });
    });

    it('should return current@10 and set __hrefFromOnClick', () => {
      rewireMethod('divertMiddleClick', () => {
        const {event, node} = data.messageJson;
        node._attributes.onclick = 'top.location.href=https://www.test_base.com';
        whereToOpen({event, href: '', node}, ['current@10']);
        expect(linksHandler._data.event.__hrefFromOnClick).toEqual('https://www.test_base.com/');
      });
    });

    it('should return current.frame@10', () => {
      rewireMethod('divertMiddleClick', () => {
        const {event, node} = data.messageJson;
        node.ownerDocument.defaultView.frameElement = true;
        node._attributes.onclick = 'top.location.href=https://www.test_base.com';
        whereToOpen({event, href: '', node}, ['current.frame@10']);
        expect(linksHandler._data.event.__hrefFromOnClick).toEqual('https://www.test_base.com/');
      });
    });

    it('should return current.frame@10 for node with onclick inside iFrame', () => {
      rewireMethod('divertMiddleClick', () => {
        const {event, href, node} = data.messageJson;
        node.ownerDocument.defaultView.frameElement = true;
        node._attributes.onclick = 'top.location.href=https://www.test_base.com';
        receiveMessage({event, href, node}, ['current.frame@10']);
      });
    });
  });

  it('should return default@11 for node with onclick inside iFrame', () => {
    const {event, href, node} = data.messageJson;
    node.ownerDocument.defaultView.frameElement = true;
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    receiveMessage({event, href, node}, ['default@11']);
  });

  it('should return default@11 for node without href and with onclick inside iFrame', () => {
    const {event, node} = data.messageJson;
    node.ownerDocument.defaultView.frameElement = true;
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    receiveMessage({event, href: '', node}, ['default@11']);
  });

  it('should return default@12 for middle click', () => {
    const {event, href, node} = data.messageJson;
    event.button = 1;
    whereToOpen({event, href, node}, ['default@12']);
  });

  it('should return tab@12 for middle click on node with href from onClick', () => {
    const {event, node} = data.messageJson;
    event.button = 1;
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    whereToOpen({event, href: '', node}, ['tab@12']);
  });

  it('should return tabshifted@12 for middle click on node with href from onClick', () => {
    const middleclick = preferences['browser.tabs.opentabfor.middleclick'];
    preferences['browser.tabs.opentabfor.middleclick'] = true;
    const {event, node} = data.messageJson;
    event.button = 1;
    event.shiftKey = true;
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    whereToOpen({event, href: '', node}, ['tabshifted@12']);

    preferences['browser.tabs.opentabfor.middleclick'] = false;
    whereToOpen({event, href: '', node}, ['window@2.1']);

    preferences['browser.tabs.opentabfor.middleclick'] = middleclick;
  });

  it('should return default@12 for right click', () => {
    const {event, href, node} = data.messageJson;
    event.button = 2;
    whereToOpen({event, href, node}, ['default@12']);
  });

  it('should return default@12 for right click on node with href from onClick', () => {
    const {event, node} = data.messageJson;
    event.button = 2;
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    whereToOpen({event, href: '', node}, ['default@12']);
  });

  it('should return default@13 when link target is in frame', () => {
    const {event, href, node} = data.messageJson;
    node._attributes.onclick = 'top.location.href=https://www.test_base.com';
    node.targetIsFrame = true;
    preferences.targetIsFrame = true;
    whereToOpen({event, href, node}, ['default@13']);

    node.targetIsFrame = false;
    preferences.targetIsFrame = true;
    whereToOpen({event, href, node}, ['default@17']);

    node.targetIsFrame = true;
    preferences.targetIsFrame = false;
    whereToOpen({event, href, node}, ['default@17']);
  });

  it('should return current@14', () => {
    rewireMethod('divertTargetedLink', () => {
      receiveMessage(data.messageJson, ['current@14']);
    });
  });

  it('should return tab@15', () => {
    rewireMethod('openExSiteLink', () => {
      const {event, href, node} = data.messageJson;
      whereToOpen({event, href, node}, ['tab@15']);
    });
  });

  it('should return tabshifted@15', () => {
    rewireMethod('openExSiteLink', () => {
      const {event, href, node} = data.messageJson;
      event.ctrlKey = true;
      event.shiftKey = true;
      whereToOpen({event, href, node}, ['tabshifted@15']);
    });
  });

  it('should return @16', () => {
    function setAllTabsLock(val) {
      backgroundRewireAPI.__Rewire__('ALL_TABS_LOCKED', val);
    }

    function runTest(message, expected) {
      preferences.opentabforLinks = 1;
      setAllTabsLock(false);
      whereToOpen(message, expected);

      preferences.opentabforLinks = 0;
      setAllTabsLock(true);
      whereToOpen(message, expected);
    }

    rewireMethod('openTabfromLink', () => {
      runTest(data.messageJson, ['default@17']);
    }, () => null);

    rewireMethod('openTabfromLink', () => {
      runTest(data.messageJson, ['default@16']);
    }, () => false);

    rewireMethod('openTabfromLink', () => {
      runTest(data.messageJson, ['tab@16']);
    }, () => true);

    rewireMethod('openTabfromLink', () => {
      const {event, href, node} = data.messageJson;
      event.ctrlKey = true;
      event.shiftKey = true;
      runTest({event, href, node}, ['tabshifted@16']);
    }, () => true);

    backgroundRewireAPI.__ResetDependency__('ALL_TABS_LOCKED');
  });
});

describe('helper methods', () => {
  const linksHandler = backgroundRewireAPI.__GetDependency__('linksHandler');
  const rewireMethod = rewireMethodFromHandler.bind(null, linksHandler);

  it('isGreasemonkeyScript should return true', () => {
    rewireMethod('isGMEnabled', () => {
      expect(linksHandler.isGreasemonkeyScript('some_file.user.js')).toBe(true);
      expect(linksHandler.isGreasemonkeyScript('some_file.xxxx.user.js')).toBe(true);
    });
  });

  it('isGreasemonkeyScript should return false', () => {
    expect(linksHandler.isGreasemonkeyScript('some_file.user.js')).toBe(false);
    rewireMethod('isGMEnabled', () => {
      expect(linksHandler.isGreasemonkeyScript('some_file.xx_user.js')).toBe(false);
    });
    rewireMethod('isGMEnabled', () => {
      expect(linksHandler.isGreasemonkeyScript('some_file.user.js')).toBe(false);
    }, () => false);
  });

  describe('suppressTabsOnFileDownload', () => {
    const method = linksHandler.suppressTabsOnFileDownload.bind(linksHandler);

    function prepareData(url) {
      const string = JSON.stringify(data.message);
      const json = JSON.parse(string.replace(/http:\/\/localhost\//g, url));
      json.event._currentURL = url;
      json.node = linksHandler.getWrappedNode(json.node);
      return json;
    }

    function runTest(prepareDataForTest) {
      const {event, href, node} = prepareDataForTest();
      linksHandler.getData(event, href, node);
      return method();
    }

    it('should return false', () => {
      const url = 'https://www.google.com/search?q=%3F+test';
      expect(runTest(() => prepareData(url))).toBe(false);

      expect(runTest(() => {
        const json = prepareData();
        json.event.button = 1;
        return json;
      })).toBe(false);

      expect(runTest(() => {
        const json = prepareData();
        json.event.ctrlKey = true;
        return json;
      })).toBe(false);

      expect(runTest(() => {
        const json = prepareData();
        json.event.metaKey = true;
        return json;
      })).toBe(false);

      expect(runTest(() => {
        const json = prepareData();
        json.href = 'http://tinderbox.mozilla.org/showlog';
        return json;
      })).toBe(false);

      expect(runTest(() => {
        const json = prepareData();
        json.href = 'http://tinderbox.mozilla.org/addnote';
        return json;
      })).toBe(false);

      expect(runTest(() => {
        const json = prepareData();
        json.href = 'javascript:top.location.href=somewhere';
        return json;
      })).toBe(false);
    });

    it('should return true for link that starts with custombutton://', () => {
      const custombuttons = global.custombuttons;
      global.custombuttons = () => true;
      expect(runTest(() => {
        const json = prepareData();
        json.event.button = 1;
        json.href = 'custombutton://some_button';
        return json;
      })).toBe(true);

      expect(runTest(() => {
        const json = prepareData();
        json.event.button = 1;
        json.href = 'https://wiki.a_tag.com/';
        return json;
      })).toBe(false);
      global.custombuttons = custombuttons;
    });

    const functions = ['install', 'installTheme', 'note', 'log'];
    for (const fn of functions) {
      it(`should return true for onclick with "return ${fn}"`, () => {
        expect(runTest(() => {
          const json = prepareData();
          json.node._attributes.onclick = `return ${fn}(aaa, bbb)`;
          return json;
        })).toBe(true);
      });
    }
  });

  describe('isUrlForDownload', () => {
    const isUrlForDownload = linksHandler.isUrlForDownload.bind(linksHandler);

    beforeEach(() => {
      preferences.enablefiletype = true;
    });

    it('should throw an error when href argument is missing', () => {
      expect(() => isUrlForDownload()).toThrow();
    });

    it('should return false when linkHref is falsy', () => {
      expect(isUrlForDownload('')).toBe(false);
    });

    it('should always return true for href starts with \'mailto:\'', () => {
      expect(isUrlForDownload('mailto:xxxx')).toBe(true);
    });

    it('should return true for links that contain file extension from our list', () => {
      preferences.filetype = 'zip';
      const base = 'github.com/username/yarn/archive/';
      const href1 = `https://${base}master.zip`;
      const href2 = `https://${base}master.zip1`;
      const href3 = `https://${base}master.zip/xxx`;
      expect(isUrlForDownload(href1)).toBe(true);
      expect(isUrlForDownload(href2)).toBe(false);
      expect(isUrlForDownload(href3)).toBe(false);
    });

    it('should return true for links that match RegExp: /&disp=(safe|attd)&/', () => {
      preferences.filetype = '/&disp=(safe|attd)&/';
      // link for download from gmail
      const gmail = 'mail.google.com/mail/?ui=2&ik=f&view=att&th=1';
      const href1 = `https://${gmail}&disp=safe&realattid=f&zw`;
      const href2 = `https://${gmail}&disp=attd&realattid=f&zw`;
      const href3 = `https://${gmail}&disp=attd1&realattid=f&zw`;
      expect(isUrlForDownload(href1)).toBe(true);
      expect(isUrlForDownload(href2)).toBe(true);
      expect(isUrlForDownload(href3)).toBe(false);
    });

    it('should return true for links that match RegExp: php?attachmentid=.*', () => {
      preferences.filetype = 'php?attachmentid=.*';
      const base = 'www.test_base.com/';
      const href1 = `https://${base}filename.php?attachmentid=aaaa&moredata=bbbb`;
      const href2 = `https://${base}filename.php?attachmentid=1111&moredata=bbbb`;
      const href3 = `https://${base}filename.somephp?attachmentid=aaaa`;
      expect(isUrlForDownload(href1)).toBe(true);
      expect(isUrlForDownload(href2)).toBe(true);
      expect(isUrlForDownload(href3)).toBe(false);
    });

    it('should return true for links that match RegExp: php?act=Attach&type=post&id=.*', () => {
      preferences.filetype = 'php?act=Attach&type=post&id=.*';
      const base = 'www.test_base.com/';
      const href1 = `https://${base}filename.php?act=attach&type=post&id=aaaa&moredata=bbbb`;
      const href2 = `https://${base}filename.php?act=attach&type=post&id=1111&moredata=bbbb`;
      const href3 = `https://${base}filename.somephp?act=attach&type=post&id=aaaa`;
      expect(isUrlForDownload(href1)).toBe(true);
      expect(isUrlForDownload(href2)).toBe(true);
      expect(isUrlForDownload(href3)).toBe(false);
    });

    it('should return true for links that match RegExp: /download\\.(php|asp)\\?*/', () => {
      preferences.filetype = '/download\\.(php|asp)\\?*/';
      const base = 'www.test_base.com/';
      const href1 = `https://${base}download.php?type=post&id=aaaa&moredata=bbbb`;
      const href2 = `https://${base}download.php?type=post&id=1111&moredata=bbbb`;
      const href3 = `https://${base}download.somephp?type=post&id=aaaa`;
      expect(isUrlForDownload(href1)).toBe(true);
      expect(isUrlForDownload(href2)).toBe(true);
      expect(isUrlForDownload(href3)).toBe(false);

      const href4 = `https://${base}download.asp?type=post&id=aaaa&moredata=bbbb`;
      const href5 = `https://${base}download.asp?type=post&id=1111&moredata=bbbb`;
      const href6 = `https://${base}download.someasp?type=post&id=aaaa`;
      expect(isUrlForDownload(href4)).toBe(true);
      expect(isUrlForDownload(href5)).toBe(true);
      expect(isUrlForDownload(href6)).toBe(false);
    });

    it('should return true for links that match RegExp: download/file\\.php', () => {
      preferences.filetype = 'download/file\\.php';
      const base = 'www.test_base.com/';
      const href1 = `https://${base}download/file.php`;
      const href2 = `https://${base}download/file.php?type=post&id=1111&moredata=bbbb`;
      const href3 = `https://${base}download/fileXphp?type=post&id=1111&moredata=bbbb`;
      const href4 = `https://${base}download.somephp?type=post&id=aaaa`;
      expect(isUrlForDownload(href1)).toBe(true);
      expect(isUrlForDownload(href2)).toBe(true);
      expect(isUrlForDownload(href3)).toBe(false);
      expect(isUrlForDownload(href4)).toBe(false);
    });

    it('should return true for links that match RegExp: /\\.xls(x)*\\?*/', () => {
      preferences.filetype = '/\\.xls(x)*(?=\\?|$)/';
      const base = 'www.test_base.com/';
      const href1 = `https://${base}filename.xls`;
      const href2 = `https://${base}filename.xls?type=post&id=1111&moredata=bbbb`;
      const href3 = `https://${base}filename.xlsx`;
      const href4 = `https://${base}filename.xlsx?type=post&id=1111&moredata=bbbb`;
      const href5 = `https://${base}filename.xls1?type=post&id=1111&moredata=bbbb`;
      const href6 = `https://${base}filename.xlstype=post&id=1111&moredata=bbbb`;
      expect(isUrlForDownload(href1)).toBe(true);
      expect(isUrlForDownload(href2)).toBe(true);
      expect(isUrlForDownload(href3)).toBe(true);
      expect(isUrlForDownload(href4)).toBe(true);
      expect(isUrlForDownload(href5)).toBe(false);
      expect(isUrlForDownload(href6)).toBe(false);
    });
  });

  describe('divertMiddleClick', () => {
    const divertMiddleClick = linksHandler.divertMiddleClick.bind(linksHandler);

    afterEach(() => {
      backgroundRewireAPI.__ResetDependency__('ALL_TABS_LOCKED');
    });

    it('should return false when middlecurrent preference is false', () => {
      preferences.middlecurrent = false;
      expect(divertMiddleClick()).toBe(false);
    });

    it('should return false when the link is not "forced" to be open in new tab', () => {
      preferences.middlecurrent = true;
      preferences.opentabforLinks = 0;
      backgroundRewireAPI.__Rewire__('ALL_TABS_LOCKED', false);
      expect(divertMiddleClick()).toBe(false);

      preferences.opentabforLinks = 2;
      backgroundRewireAPI.__Rewire__('ALL_TABS_LOCKED', false);
      linksHandler._data = {isLinkToExternalDomain: false};
      expect(divertMiddleClick()).toBe(false);
    });

    it('should return true', () => {
      preferences.middlecurrent = true;

      preferences.opentabforLinks = 1;
      linksHandler._data = {event: {button: 1}};
      expect(divertMiddleClick()).toBe(true);

      preferences.opentabforLinks = 2;
      linksHandler._data = {event: {button: 0, ctrlKey: true}, isLinkToExternalDomain: true};
      expect(divertMiddleClick()).toBe(true);

      preferences.opentabforLinks = 0;
      backgroundRewireAPI.__Rewire__('ALL_TABS_LOCKED', true);
      linksHandler._data = {event: {button: 0, metaKey: true}};
      expect(divertMiddleClick()).toBe(true);
    });
  });

  describe('divertTargetedLink', () => {
    const divertTargetedLink = linksHandler.divertTargetedLink.bind(linksHandler);

    afterEach(() => {
      backgroundRewireAPI.__ResetDependency__('ALL_TABS_LOCKED');
    });

    function prepareData(props = {}) {
      backgroundRewireAPI.__Rewire__('ALL_TABS_LOCKED', false);
      preferences.linkTarget = true;
      preferences.opentabforLinks = 0;
      const base = {
        href: 'http://tabmixplus.org',
        targetAttr: '_blank',
        event: {button: 0},
      };
      linksHandler._data = {...base, ...props};
    }

    it('should return false when href starts with javascript', () => {
      prepareData({href: 'javascript:xxxxx'});
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when href starts with data', () => {
      prepareData({hrefFromOnClick: 'data:xxxxx'});
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when there is no target attr', () => {
      prepareData({targetAttr: ''});
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when ctrlKey pressed', () => {
      prepareData({event: {ctrlKey: true}});
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when metaKey pressed', () => {
      prepareData({event: {metaKey: true}});
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when linkTarget preference is false', () => {
      prepareData();
      preferences.linkTarget = false;
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when target attr is one of: _self _parent _top _content _main', () => {
      ['_self', '_parent', '_top', '_content', '_main'].forEach(att => {
        prepareData({targetAttr: att});
        expect(divertTargetedLink()).toBe(false);
      });
    });

    it('should return false when the tab is locked', () => {
      prepareData();
      backgroundRewireAPI.__Rewire__('ALL_TABS_LOCKED', true);
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when links are forced to open in new tab', () => {
      prepareData();
      preferences.opentabforLinks = 1;
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when links to other site are forced to open in new tab', () => {
      prepareData({isLinkToExternalDomain: true});
      preferences.opentabforLinks = 2;
      expect(divertTargetedLink()).toBe(false);
    });

    it('should return false when linksHandler.checkOnClick is true', () => {
      rewireMethod('checkOnClick', () => {
        prepareData();
        expect(divertTargetedLink()).toBe(false);
      }, () => true);
    });

    it('should return true when linksHandler.checkOnClick is false', () => {
      rewireMethod('checkOnClick', () => {
        prepareData();
        expect(divertTargetedLink()).toBe(true);
      }, () => false);
    });
  });

  describe('openExSiteLink', () => {
    const openExSiteLink = linksHandler.openExSiteLink.bind(linksHandler);

    function prepareData(props = {}) {
      const base = {href: 'http://tabmixplus.org'};
      linksHandler._data = {...base, ...props};
    }

    it('should return false when opentabforLinks is not 2', () => {
      preferences.opentabforLinks = 0;
      prepareData();
      expect(openExSiteLink()).toBe(false);

      preferences.opentabforLinks = 1;
      prepareData();
      expect(openExSiteLink()).toBe(false);
    });

    it('should return false when currentURL is in Tabmix.newTabUrls', () => {
      preferences.opentabforLinks = 2;
      prepareData({currentURL: 'about:newtab'});
      expect(openExSiteLink()).toBe(false);
    });

    it('should return false when GoogleComLink return true', () => {
      rewireMethod('GoogleComLink', () => {
        preferences.opentabforLinks = 2;
        prepareData();
        expect(openExSiteLink()).toBe(false);
      });
    });

    it('should return false when checkOnClick return true', () => {
      rewireMethod('checkOnClick', () => {
        preferences.opentabforLinks = 2;
        prepareData();
        expect(openExSiteLink()).toBe(false);
      });
    });

    it('should return false when links protocol is NOT http[s] or about', () => {
      preferences.opentabforLinks = 2;
      prepareData({href: 'file:///C:/Users/X/Desktop/test.html'});
      expect(openExSiteLink()).toBe(false);
    });

    it('should return false when conditions are NOT met', () => {
      // current page and link target are in the same domain and the link node
      // don't have an 'onmousedown' attribute that contains the text 'return rwt'
      preferences.opentabforLinks = 2;
      prepareData({
        isLinkToExternalDomain: false,
        wrappedNode: linksHandler.getWrappedNode({_attributes: {onmousedown: 'return x'}}),
      });
      expect(openExSiteLink()).toBe(false);
    });

    it('should return true when conditions are met - isLinkToExternalDomain', () => {
      // current page and link target are NOT in the same domain
      preferences.opentabforLinks = 2;
      prepareData({isLinkToExternalDomain: true});
      expect(openExSiteLink()).toBe(true);
    });

    it('should return true when conditions are met - onmousedown', () => {
      // link node have an 'onmousedown' attribute that contains the text 'return rwt'
      preferences.opentabforLinks = 2;
      prepareData({wrappedNode: linksHandler.getWrappedNode({_attributes: {onmousedown: 'return rwt'}})});
      expect(openExSiteLink()).toBe(true);
    });
  });

  describe('openTabfromLink', () => {
    const openTabfromLink = linksHandler.openTabfromLink.bind(linksHandler);

    function prepareData(props = {}) {
      const base = {
        href: 'http://tabmixplus.org',
        currentURL: '',
      };
      linksHandler._data = {...base, ...props};
    }

    it('should return false when currentURL is in Tabmix.newTabUrls', () => {
      prepareData({currentURL: 'about:newtab'});
      expect(openTabfromLink()).toBe(false);
    });

    it('should return null when GoogleComLink return true', () => {
      rewireMethod('GoogleComLink', () => {
        prepareData();
        expect(openTabfromLink()).toBe(null);
      });
    });

    it('should return null when links protocol is NOT http[s] or about', () => {
      prepareData({href: 'file:///C:/Users/X/Desktop/test.html'});
      expect(openTabfromLink()).toBe(null);

      prepareData({hrefFromOnClick: 'file:///C:/Users/X/Desktop/test.html'});
      expect(openTabfromLink()).toBe(null);
    });

    it('should return false when href is to facebook chat or settings', () => {
      prepareData({href: 'https://www.facebook.com/ajax'});
      expect(openTabfromLink()).toBe(false);

      prepareData({href: 'https://www.facebook.com/settings'});
      expect(openTabfromLink()).toBe(false);
    });

    describe('on youtube', () => {
      it('should return false when href from onclick is to the same video on different time', () => {
        prepareData({
          currentURL: 'https://www.youtube.com/watch?v=someID&t=100s',
          hrefFromOnClick: 'https://www.youtube.com/watch?v=someID&t=200s',
        });
        expect(openTabfromLink()).toBe(false);
      });

      it('should return true when href from onclick is to a different video', () => {
        prepareData({
          currentURL: 'https://www.youtube.com/watch?v=someID_0&t=100s',
          hrefFromOnClick: 'https://www.youtube.com/watch?v=someID_1&t=100s',
        });
        expect(openTabfromLink()).toBe(true);
      });

      it('should return false when href is to the same video on different time', () => {
        prepareData({
          currentURL: 'https://www.youtube.com/watch?v=someID&t=100s',
          href: 'https://www.youtube.com/watch?v=someID&t=200s',
        });
        expect(openTabfromLink()).toBe(false);
      });

      it('should return true when href is to a different video', () => {
        prepareData({
          currentURL: 'https://www.youtube.com/watch?v=someID_0&t=100s',
          href: 'https://www.youtube.com/watch?v=someID_1&t=100s',
        });
        expect(openTabfromLink()).toBe(true);
      });
    });

    it('should return false when href from onclick is to the same page', () => {
      prepareData({
        currentURL: 'http://tabmixplus.org/index.html/',
        hrefFromOnClick: 'http://tabmixplus.org/index.html/#news',
      });
      expect(openTabfromLink()).toBe(false);
    });

    it('should return true when href from onclick is to a different page', () => {
      prepareData({
        currentURL: 'http://tabmixplus.org/index.html/',
        hrefFromOnClick: 'http://tabmixplus.org/news.html/#news',
      });
      expect(openTabfromLink()).toBe(true);
    });

    it('should return false when href is to the same page', () => {
      prepareData({
        currentURL: 'http://tabmixplus.org/index.html/',
        href: 'http://tabmixplus.org/index.html/#news',
      });
      expect(openTabfromLink()).toBe(false);
    });

    it('should return true when href is to a different page', () => {
      prepareData({
        currentURL: 'http://tabmixplus.org/index.html/',
        href: 'http://tabmixplus.org/news.html/#news',
      });
      expect(openTabfromLink()).toBe(true);
    });

    it('should return null when href starts with javascript', () => {
      prepareData({href: 'javascript:xxxxx'});
      expect(openTabfromLink()).toBe(null);
    });

    it('should return null when href starts with data', () => {
      prepareData({href: 'data:xxxxx'});
      expect(openTabfromLink()).toBe(null);
    });

    it('should return null when checkOnClick return true', () => {
      rewireMethod('checkOnClick', () => {
        prepareData();
        expect(openTabfromLink()).toBe(null);
        expect(linksHandler.checkOnClick).toBeCalledWith(true);
      });
    });
  });

  describe('GoogleComLink', () => {
    const GoogleComLink = linksHandler.GoogleComLink.bind(linksHandler);

    function prepareData(props = {}) {
      const base = {
        href: 'https://github.com/username/yarn/archive/',
        currentURL: 'https://www.google.com/search?safe=off',
      };
      linksHandler._data = {...base, ...props};
    }

    it('should return false when current page is not google', () => {
      prepareData({currentURL: 'http://tabmixplus.org'});
      expect(GoogleComLink()).toBe(false);
    });

    it('should return true when current page is google calendar', () => {
      prepareData({currentURL: 'https://calendar.google.com/calendar/render/day/'});
      expect(GoogleComLink()).toBe(true);
    });

    it('should return true for link to google options', () => {
      const wrappedNode = linksHandler.getWrappedNode({pathname: '/intl/en/options/'});
      prepareData({wrappedNode});
      expect(GoogleComLink()).toBe(true);

      prepareData({wrappedOnClickNode: wrappedNode});
      expect(GoogleComLink()).toBe(true);
    });

    it('should return true for link to google search', () => {
      const wrappedNode = linksHandler.getWrappedNode({pathname: '/search'});
      prepareData({wrappedNode});
      expect(GoogleComLink()).toBe(true);

      prepareData({wrappedOnClickNode: wrappedNode});
      expect(GoogleComLink()).toBe(true);
    });

    it('should return true for link with pathname to google apps', () => {
      const list = ['/preferences', '/advanced_search', '/language_tools', '/profiles',
        '/accounts/Logout', '/accounts/ServiceLogin', '/u/2/stream/all'];

      list.forEach(path => {
        const wrappedNode = linksHandler.getWrappedNode({pathname: path});
        prepareData({wrappedNode});
        expect(GoogleComLink()).toBe(true);

        prepareData({wrappedOnClickNode: wrappedNode});
        expect(GoogleComLink()).toBe(true);
      });
    });

    it('should return true for link with host to google service', () => {
      const list = [
        'profiles.google.com',
        'accounts.google.com',
        'groups.google.com',
        'news.google.com',
      ];

      list.forEach(host => {
        const wrappedNode = linksHandler.getWrappedNode({host});
        prepareData({wrappedNode});
        expect(GoogleComLink()).toBe(true);

        prepareData({wrappedOnClickNode: wrappedNode});
        expect(GoogleComLink()).toBe(true);
      });
    });
  });

  describe('checkAttr', () => {
    const checkAttr = linksHandler.checkAttr.bind(linksHandler);
    it('should return true', () => {
      expect(checkAttr('some string', 'some st')).toBe(true);
    });

    it('should return false', () => {
      expect(checkAttr({value: 'some string'}, 'some st')).toBe(false);
    });
  });

  describe('isLinkToExternalDomain', () => {
    const isLinkToExternalDomain = linksHandler.isLinkToExternalDomain.bind(linksHandler);
    it('should return null when 2nd argument don\'t have domain', () => {
      expect(isLinkToExternalDomain('xx', 'yy')).toBe(null);
      expect(isLinkToExternalDomain('', '')).toBe(null);
      expect(isLinkToExternalDomain('a.d.e', '')).toBe(null);
    });

    it('should return true when 1st argument is invalid url if the 2nd argument is valid url', () => {
      expect(isLinkToExternalDomain('tabmixplus', 'tabmixplus.org')).toBe(true);
      expect(isLinkToExternalDomain('', 'a.d.e')).toBe(true);
    });

    it('should return false', () => {
      expect(isLinkToExternalDomain('tabmixplus.org', 'http://tabmixplus.org/forum/viewforum.php?f=1')).toBe(false);
      expect(isLinkToExternalDomain('https://translate.google.uk', 'https://www.google.uk')).toBe(false);
      expect(isLinkToExternalDomain('https://translate.google.com', 'https://www.google.com')).toBe(false);
    });

    it('should return true', () => {
      expect(isLinkToExternalDomain('tabmixplus.org', 'https://www.google.com')).toBe(true);
      expect(isLinkToExternalDomain('https://translate.google.uk', 'https://www.google.com')).toBe(true);
    });

    describe('getBaseDomain', () => {
      const getBaseDomain = linksHandler.getBaseDomain.bind(linksHandler);
      it('should return domain name from url', () => {
        expect(getBaseDomain('https://account.google.com')).toEqual('google.com');
        expect(getBaseDomain('tabmixplus.org/forum')).toEqual('tabmixplus.org');
        expect(getBaseDomain(new URL('http://tabmixplus.org/forum'))).toEqual('tabmixplus.org');
      });

      it('should return null for url that includes auth?', () => {
        expect(getBaseDomain('https://account.google.com/auth?id=1')).toEqual(null);
      });

      it('should return null for url that starts with file:', () => {
        expect(getBaseDomain('file:///C:/Users/X/Desktop/test.html')).toEqual('local_file');
      });

      it('should return null for invalid url', () => {
        expect(getBaseDomain('http:')).toEqual(null);
        expect(getBaseDomain('')).toEqual(null);
        expect(getBaseDomain({})).toEqual(null);
      });

      it('should return domain name from url with redirect', () => {
        expect(getBaseDomain('http://tabmixplus.org/r/?https://account.google.com/')).toEqual('google.com');
        expect(getBaseDomain('https://account.google.com/somepath/?url=http://tabmixplus.org/')).toEqual('tabmixplus.org');
      });

      it('should return null from invalid redirect', () => {
        expect(getBaseDomain('http://tabmixplus.org/r/?https:')).toEqual(null);
        expect(getBaseDomain('https://account.google.com/somepath/?url=http:/')).toEqual(null);
      });

      it('should return null for non http url', () => {
        expect(getBaseDomain('about:config')).toEqual(null);
        expect(getBaseDomain('ftp://tabmixplus.org/')).toEqual(null);
      });
    });

    describe('getter', () => {
      function testGetter(href, attributes, currentURL) {
        const {event, node} = data.messageJson;
        node._attributes = attributes;
        event._currentURL = currentURL;
        const wrappedNode = linksHandler.getWrappedNode(node);
        if (node._attributes.href) {
          node.ownerDocument.URL = currentURL;
          linksHandler.getData(event, href, wrappedNode);
        } else {
        // for complete coverage in
        // curpage = node.ownerDocument.URL || this.currentURL;
          node.ownerDocument.URL = '';
          // test wrappedOnClickNode
          linksHandler.getHrefFromOnClick(event, null, wrappedNode, node._attributes.onclick);
          linksHandler.getData(event, href, null, wrappedNode);
        }
        return linksHandler._data.isLinkToExternalDomain;
      }

      it('should return true', () => {
        const href = 'http://tabmixplus.org/forum/viewforum.php?f=1';
        const currentURL = 'https://www.google.com';
        expect(testGetter(href, {href}, currentURL)).toEqual(true);

        const attributes = {onclick: `top.location.href=${href}`};
        expect(testGetter('', attributes, currentURL)).toEqual(true);

        const href1 = 'https://translate.google.uk';
        expect(testGetter(href1, {href1}, currentURL)).toEqual(true);

        const attributes1 = {onclick: `top.location.href=${href1}`};
        expect(testGetter('', attributes1, currentURL)).toEqual(true);
      });

      it('should return false', () => {
        const href = 'https://translate.google.com';
        const currentURL = 'https://www.google.com';
        expect(testGetter(href, {href}, currentURL)).toEqual(false);

        const attributes = {onclick: `top.location.href=${href}`};
        expect(testGetter('', attributes, currentURL)).toEqual(false);

        const href1 = 'https://www.youtube.com/channel/UCtb40EQj2inp8zuaQlLx3iQ';
        const currentURL1 = 'https://www.youtube.com/watch?v=DwTPbtWkRHQ&t=57s&index=3&list=WL';
        expect(testGetter(href1, {href1}, currentURL1)).toEqual(false);

        const attributes1 = {onclick: `top.location.href=${href1}`};
        expect(testGetter('', attributes1, currentURL1)).toEqual(false);
      });
    });
  });

  describe('checkOnClick', () => {
    const checkOnClick = linksHandler.checkOnClick.bind(linksHandler);

    function prepareData(onclick = '') {
      linksHandler._data = {onclick};
    }

    it('should return false when there is no onclick', () => {
      prepareData();
      expect(checkOnClick()).toEqual(false);
    });

    it('should return false when onclick NOT starts with string from the list', () => {
      prepareData('window.closing');
      expect(checkOnClick()).toEqual(false);
    });

    it('should return true when onclick starts with string from the list', () => {
      const list = [
        'window.open',
        'NewWindow',
        'PopUpWin',
        'return ',
      ];
      list.forEach(str => {
        prepareData(str);
        expect(checkOnClick()).toEqual(true);
      });
    });

    it('should return true when called with argument true and onclick starts with string from the list', () => {
      const list = [
        'openit',
        'some code this.target="_Blank"',
        'some code return false',
      ];
      list.forEach(str => {
        prepareData(str);
        expect(checkOnClick(true)).toEqual(true);
      });
    });
  });

  describe('getHrefFromOnClick', () => {
    const getHrefFromOnClick = linksHandler.getHrefFromOnClick.bind(linksHandler);

    const mock = (a, b, c, result) => {
      result.__hrefFromOnClick = c;
    };

    it('should return event.__hrefFromOnClick when it was already set', () => {
      rewireMethod('_hrefFromOnClick', () => {
        expect(getHrefFromOnClick({__hrefFromOnClick: 'xxx'})).toEqual('xxx');
        expect(getHrefFromOnClick({__hrefFromOnClick: null})).toEqual(null);
        expect(getHrefFromOnClick({__hrefFromOnClick: 'return true'})).toEqual('return true');
        expect(linksHandler._hrefFromOnClick).toBeCalledTimes(0);
      });
    });

    it('should return null when there is no onclick', () => {
      rewireMethod('_hrefFromOnClick', () => {
        const event = {};
        const node = linksHandler.getWrappedNode({parentNode: {_attributes: {}}});
        expect(getHrefFromOnClick(event, '', node)).toEqual(null);
        expect(linksHandler._hrefFromOnClick).toBeCalledTimes(0);
      });
    });

    it('should call _hrefFromOnClick when there is onclick and return href when _hrefFromOnClick set result', () => {
      rewireMethod('_hrefFromOnClick', () => {
        const event = {};
        expect(getHrefFromOnClick(event, '', null, 'onclick from node')).toEqual('onclick from node');
        expect(event).toEqual({__hrefFromOnClick: 'onclick from node'});
        expect(linksHandler._hrefFromOnClick).toBeCalledTimes(1);
      }, mock);
    });

    it('should call _hrefFromOnClick and return null when _hrefFromOnClick does not set result', () => {
      rewireMethod('_hrefFromOnClick', () => {
        const event = {};
        expect(getHrefFromOnClick(event, '', null, 'onclick from node')).toEqual(null);
        expect(event).toEqual({__hrefFromOnClick: null});
        expect(linksHandler._hrefFromOnClick).toBeCalledTimes(1);
      }, () => {});
    });

    it('should call _hrefFromOnClick when there is onclick on parentNode and return href when _hrefFromOnClick set result', () => {
      rewireMethod('_hrefFromOnClick', () => {
        const event = {};
        const node = linksHandler.getWrappedNode({parentNode: {_attributes: {onclick: 'onclick from parentNode'}}});
        expect(getHrefFromOnClick(event, '', node)).toEqual('onclick from parentNode');
        expect(event).toEqual({__hrefFromOnClick: 'onclick from parentNode'});
        expect(linksHandler._hrefFromOnClick).toBeCalledTimes(1);
      }, mock);
    });
  });

  describe('_hrefFromOnClick', () => {
    const _hrefFromOnClick = linksHandler._hrefFromOnClick.bind(linksHandler);

    it('should NOT set __hrefFromOnClick on result', () => {
      const result = {__hrefFromOnClick: null};
      const onclick = 'new.location.href=42';
      _hrefFromOnClick('', {}, onclick, result);
      expect(result).toEqual({__hrefFromOnClick: null});
    });

    it('should throw an error when new URL failed', () => {
      const result = {__hrefFromOnClick: null};
      const onclick = 'location.href=this.firstChild.href;';

      const originalConsoleError = global.console.error;
      global.console.error = jest.fn();
      _hrefFromOnClick('', {}, onclick, result);
      expect(global.console.error).toBeCalledTimes(1);
      const arg = global.console.error.mock.calls[0][0];
      expect(arg).toContain('unexpected error from new URL');
      expect(arg).toContain('TypeError [ERR_INVALID_URL]: Invalid URL:');
      global.console.error = originalConsoleError;
    });

    it('should set __hrefFromOnClick from href argument for forum/ucp.php', () => {
      const result = {__hrefFromOnClick: null};
      const baseURI = 'http://tabmixplus.org';
      const onclick = 'location.href=this.firstChild.href;';
      const href = 'forum_ucp_href';
      _hrefFromOnClick(href, {baseURI}, onclick, result);
      expect(result).toEqual({__hrefFromOnClick: `${baseURI}/${href}`});
    });

    it('should NOT set __hrefFromOnClick when links protocol is NOT http[s] or about', () => {
      const result = {__hrefFromOnClick: null};
      const baseURI = 'http://tabmixplus.org';
      const onclick = 'location.href=protocolX:www.domain.com;';
      _hrefFromOnClick('', {baseURI}, onclick, result);
      expect(result).toEqual({__hrefFromOnClick: null});
    });

    it('should NOT set __hrefFromOnClick when the href point to a different address from the href we extract from the onclick', () => {
      const result = {__hrefFromOnClick: null};
      const baseURI = 'http://tabmixplus.org';
      const onclick = 'location.href=forum/viewtopic.php?f=1;';
      const href = 'http://tabmixplus.org/forum/viewtopic.php?f=1&t=19953';
      _hrefFromOnClick(href, {baseURI}, onclick, result);
      expect(result).toEqual({__hrefFromOnClick: 'http://tabmixplus.org/forum/viewtopic.php?f=1'});

      const result1 = {__hrefFromOnClick: null};
      const onclick1 = 'location.href=https://www.google.com';
      _hrefFromOnClick(href, {baseURI}, onclick1, result1);
      expect(result1).toEqual({__hrefFromOnClick: null});

      const result2 = {__hrefFromOnClick: null};
      const onclick2 = 'location.href=https://www.google.com';
      const href2 = 'javascript: window.location.href=xxx';
      _hrefFromOnClick(href2, {baseURI}, onclick2, result2);
      expect(result2).toEqual({__hrefFromOnClick: 'https://www.google.com/'});
    });

    it('should set __hrefFromOnClick when there is no href and the href we extract from the onclick is valid', () => {
      const result = {__hrefFromOnClick: null};
      const baseURI = 'http://tabmixplus.org';
      const onclick = 'location.href=forum/viewtopic.php?f=1;';
      _hrefFromOnClick('', {baseURI}, onclick, result);
      expect(result).toEqual({__hrefFromOnClick: 'http://tabmixplus.org/forum/viewtopic.php?f=1'});

      const result1 = {__hrefFromOnClick: null};
      const onclick1 = 'location.href=https://www.google.com';
      _hrefFromOnClick('', {baseURI}, onclick1, result1);
      expect(result1).toEqual({__hrefFromOnClick: 'https://www.google.com/'});
    });
  });
});

describe('onMessage', () => {
  it('should return true', () => {
    const onMessage = backgroundRewireAPI.__GetDependency__('onMessage');
    testFunctions('asyncResponse', ({asyncResponse}) => {
      expect(onMessage(1, 2, 3)).toEqual(true);
      expect(asyncResponse).toBeCalledTimes(1);
      expect(asyncResponse).toBeCalledWith(1, 2, 3);
    });
  });

  it('asyncResponse should return true', () => {
    const asyncResponse = backgroundRewireAPI.__GetDependency__('asyncResponse');
    const sendResponse = jest.fn();
    const linksHandler = backgroundRewireAPI.__GetDependency__('linksHandler');
    const rewireMethod = rewireMethodFromHandler.bind(null, linksHandler);

    return rewireMethod('receiveMessage', async receiveMessage => {
      await asyncResponse({test: 'test1', event: {}}, {}, sendResponse);
      expect(receiveMessage).toBeCalledTimes(1);
      expect(receiveMessage).toBeCalledWith({
        test: 'test1',
        event: {
          _currentURL: '',
          _pinned: false,
        },
      });
      expect(sendResponse).toBeCalledTimes(1);
      expect(sendResponse).toBeCalledWith('result for test1');
    }, msg => `result for ${msg.test}`);
  });
});
