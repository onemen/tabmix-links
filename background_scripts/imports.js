/* eslint-env node */
'use strict';

const {getDomain} = require('tldjs');

window.getDomain = getDomain;
