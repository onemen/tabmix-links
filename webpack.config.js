/* eslint-env node */
'use strict';

const path = require('path');

module.exports = {
  entry: {
    // eslint-disable-next-line
    background_scripts: './background_scripts/imports.js',
  },
  output: {
    path: path.resolve(__dirname, 'addon'),
    filename: '[name]/index.js',
  },
  node: false,
};
